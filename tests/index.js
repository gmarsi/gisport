import React, { Component } from 'react';
import { View, TextInput, Image, Animated, Keyboard, ScrollView, Text } from 'react-native';
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL} from './styles';
import logo from './logo.png';

class ProfileRow extends Component {
    constructor(props) {
        super(props)
        this.state = {
                editable: this.props.editable
            }
    }

    render() {
        return(
            <View style = {{alignItems: 'flex-start', width: '100%', marginBottom: 15,}}>             
                <Text style = {{fontSize: 16, color: '#000'}}>{this.props.title}</Text>
                <View style = {{width: '100%', height: 50,   marginTop: 5, }}>
                    <TextInput
                        style={styles.input}
                        onChangeText={this.props.onChange}
                        value={this.props.value}
                        editable = {this.props.editable}
                        underlineColorAndroid='transparent'
                        onFocus = {this.props.onFocus}
                        clearButtonMode = 'always'
                        returnKeyType = 'done'
                        keyboardType = {this.props.keyboardType || 'default'}


                      />
                </View>                
            </View>
        )
    }
}


class Demo extends Component {
  constructor(props) {
    super(props);

    this.keyboardHeight = new Animated.Value(0);
    this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
  }

  componentWillMount () {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  keyboardWillShow = (event) => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: 100,
        toValue: event.endCoordinates.height + 20,
      }),
      Animated.timing(this.imageHeight, {
        duration: event.duration,
        toValue: IMAGE_HEIGHT_SMALL,
      }),
    ]).start();
  };

  keyboardWillHide = (event) => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: event.duration,
        toValue: 0,
      }),
      Animated.timing(this.imageHeight, {
        duration: event.duration,
        toValue: IMAGE_HEIGHT,
      }),
    ]).start();
  };

  render() {
    return (
      <Animated.View style={[styles.container, { paddingBottom: this.keyboardHeight }]}>
        <ScrollView style = {{width: '100%'}}>
            <View style = {{width: '100%', alignItems: 'center',}}>
                <Animated.Image source={logo} style={[styles.logo, { height: this.imageHeight }]} />
            </View>
            <ProfileRow title = {'Name'} />
            <ProfileRow title = {'Name'} />
            <ProfileRow title = {'Name'} />
            <ProfileRow title = {'Name'} />
            <ProfileRow title = {'Name'} />
        </ScrollView>
      </Animated.View>
    );
  }
};

export default Demo;