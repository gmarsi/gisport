const API = {
    "chest": 
        [{
            'id': 'C1',
            'name': 'Barbell Front Raise Pullover',
            'level': '4',
            'group': 'Chest',
            'primaryMuscle': 'Pectoralis major, Serratus anterior',
            'secondaryMuscle' : 'Latissimus dorsi',
            'alternatives' : '',
            'description' : "Grasp a moderately weighted dumbbell so your palms are flat against the underside of the top plates and your thumbs are around the bar. Lie on your back across a flat bench so only your upper back and shoulders are in contact with the bench. Your feet should be set about shoulder-width apart and your head should hang slightly downward. With the dumbbell supported at arm's length directly about your chest, bend your arms about 15 degrees and keep them bent throughout the movement. Slowly lower the dumbbell backward and downward in a semicircle arc to as low a position as is comfortably possible. Raise it slowly back along the same arc to the starting point, and repeat for the required number of repetitions.",
            'minRep': '6',
            'maxRep': '12',
            'image': [
                require('../img/exercices/chest/barbell-front-raise-pullover-1.jpg'),
                require('../img/exercices/chest/barbell-front-raise-pullover-2.jpg'),
            ]
        },
        {
            'id': 'C2',
            'name': 'Barbell Neck Press',
            'level': '4',
            'primaryMuscle': 'Pectoralis major, Anterior deltoid',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "The movement is very similar to benchpressing with a barbell , however, the weight is brought down to the neck at a lower point. Hold the bar and lay down on a bench. Hold the weights next to the neck, and press it up till the arms are stretched. Let the weight slowly and controlled down.",
            'group': 'Chest',
            'minRep': '6',
            'maxRep': '12',
            'image': [
                require('../img/exercices/chest/barbell-neck-press-1.jpg'),
                require('../img/exercices/chest/barbell-neck-press-2.jpg'),
            ]
        },
        {
            'id': 'C3',
            'name': 'Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major, Anterior deltoid',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "The movement is very similar to benchpressing with a dumbbell, however, the weight is brought down to the chest at a lower point. Hold the bar and lay down on a bench. Hold the weights next to the chest, at the height of your nipples and press it up till the arms are stretched. Let the weight slowly and controlled down.",
            'group': 'Chest',
            'minRep': '6',
            'maxRep': '12',
            'image': [
                require('../img/exercices/chest/bench-press-1.jpg'),
                require('../img/exercices/chest/bench-press-2.jpg'),
            ]
        },
        {
            'id': 'C4',
            'name': 'Bench Press Dumbell',
            'level': '1',
            'primaryMuscle': 'Pectoralis major, Anterior deltoid',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "The movement is very similar to benchpressing with a barbell, however, the weight is brought down to the chest at a lower point. Hold two dumbbells and lay down on a bench. Hold the weights next to the chest, at the height of your nipples and press them up till the arms are stretched. Let the weight slowly and controlled down.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/bench-press-dumbbell-1.jpg'),
                require('../img/exercices/chest/bench-press-dumbbell-2.jpg'),
            ]
        },
        {
            'id': 'C5',
            'name': 'Bent Arm Pullover',
            'level': '1',
            'primaryMuscle': 'Pectoralis major, Serratus anterior',
            'secondaryMuscle' : 'Latissimus dorsi',
            'alternatives' : '',
            'description' : "Grasp a moderately weighted barbell so your palms are flat against the underside of the top plates and your thumbs are around the bar. Lie on your back across a flat bench so only your upper back and shoulders are in contact with the bench. Your feet should be set about shoulder-width apart and your head should hang slightly downward. With the barbell supported at arm's length directly about your chest, bend your arms about 15 degrees and keep them bent throughout the movement. Slowly lower the dumbbell backward and downward in a semicircle arc to as low a position as is comfortably possible. Raise it slowly back along the same arc to the starting point, and repeat for the required number of repetitions.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/bent-arm-pullover-1.jpg'),
                require('../img/exercices/chest/bent-arm-pullover-2.jpg'),
            ]
        },
        {
            'id': 'C6',
            'name': 'Butterfly Machine',
            'level': '1',
            'primaryMuscle': 'Pectoralis major, Anterior deltoid',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "Sit on the butterfly machine, the feet have a good contact with the floor, the upper arms are parallel to the floor. Press your arms together till the handles are practically together (but aren't!). Go slowly back. The weights should stay all the time in the air.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/butterfly-machine-1.jpg'),
                require('../img/exercices/chest/butterfly-machine-1.jpg'),
            ]
        },
        {
            'id': 'C7',
            'name': 'Chest Dips',
            'level': '3',
            'primaryMuscle': 'Pectoralis major, Anterior deltoid',
            'secondaryMuscle' : 'Triceps brachii, Rectus abdominis',
            'alternatives' : '',
            'description' : "Hold onto the bars at a narrow place (if they are not parallel) and press yourself up, but don't stretch the arms completely, so the muscles stay during the whole exercise under tension. Now bend the arms and go down as much as you can, keeping the elbows always pointing back, At this point, you can make a short pause before repeating the movement.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/chest-dips-1.jpg'),
                require('../img/exercices/chest/chest-dips-2.jpg'),
            ]
        },
        {
            'id': 'C8',
            'name': 'Close-grip Barbell Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Perform a typical bench press, but hold the bar with your hands approximately shoulder-width apart and keep your elbows close to your body.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/close-grip-barbell-bench-press-1.jpg'),
                require('../img/exercices/chest/close-grip-barbell-bench-press-2.jpg'),
            ],

        },
        {
            'id': 'C9',
            'name': 'Decline Barbell Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Lay down on a decline bench, the barbell should be directly above your eyes, the knees are somewhat angled and the feet are firmly on the floor. Concentrate, breath deeply and grab the barbell more than shoulder wide. Bring it slowly down till it briefly touches your chest at the height of your nipples. Push the barbell up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/decline-barbell-bench-press-1.jpg'),
                require('../img/exercices/chest/decline-barbell-bench-press-2.jpg'),
            ],

        },
        {
            'id': 'C10',
            'name': 'Decline Chest Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Similar to Decline Barbell Bench Press.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/decline-chest-press-1.jpg'),
                require('../img/exercices/chest/decline-chest-press-2.jpg'),
            ],

        },
        {
            'id': 'C11',
            'name': 'Decline Dumbell Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Lay down on a decline bench, the dumbbells should be directly above your eyes, the knees are somewhat angled and the feet are firmly on the floor. Concentrate, breath deeply and grab the dumbbells more than shoulder wide. Bring it slowly down till it briefly touches your chest at the height of your nipples. Push the dumbbells up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/decline-dumbbell-bench-press-1.jpg'),
                require('../img/exercices/chest/decline-dumbbell-bench-press-2.jpg'),
            ],

        },
        {
            'id': 'C12',
            'name': 'Decline Dubmell Flys',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/decline-dumbbell-flys-1.jpg'),
                require('../img/exercices/chest/decline-dumbbell-flys-2.jpg'),
            ],

        },
        {
            'id': 'C13',
            'name': 'Dumbbell Flys',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/dumbbell-flys-1.jpg'),
                require('../img/exercices/chest/dumbbell-flys-2.jpg'),
            ],

        },
                {
            'id': 'C14',
            'name': 'Dumbbell Incline Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/dumbbell-incline-bench-press-1.jpg'),
                require('../img/exercices/chest/dumbbell-incline-bench-press-2.jpg'),
            ],

        },

        
                {
            'id': 'C15',
            'name': 'Bench Cable Flys',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/flat-bench-cable-flys-1.jpg'),
                require('../img/exercices/chest/flat-bench-cable-flys-2.jpg'),
            ],

        },
        
                {
            'id': 'C16',
            'name': 'Hammer Grip Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/hammer-grip-incline-bench-press-1.jpg'),
                require('../img/exercices/chest/hammer-grip-incline-bench-press-2.jpg'),
            ],

        },
                {
            'id': 'C17',
            'name': 'High Cable Crossover',
            'level': '1',
            'primaryMuscle': 'Pectoralis major, Anterior deltoid',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Begin with cables at about shoulder height, one in each hand. Take a step forward so that one foot is in front of the other, for stability, and so that there is tension on the cables. Bring hands together in front of you. Try to make your hands overlap (so that the cables cross) a few inches.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/cable-crossover-1.jpg'),
                require('../img/exercices/chest/cable-crossover-2.jpg'),
            ]
        },
        
             {
            'id': 'C18',
            'name': 'Incline Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/incline-bench-press-1.jpg'),
                require('../img/exercices/chest/incline-bench-press-2.jpg'),
            ],

        },
        
                {
            'id': 'C19',
            'name': 'Incline Bench Press with Bands',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/incline-bench-press-with-bands-1.jpg'),
                require('../img/exercices/chest/incline-bench-press-with-bands-2.jpg'),
            ],

        },
        
                {
            'id': 'C20',
            'name': 'Incline Cable Flys',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/incline-cable-flys-1.jpg'),
                require('../img/exercices/chest/incline-cable-flys-2.jpg'),
            ],

        
        },
                {
            'id': 'C21',
            'name': 'Incline Chest Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/incline-chest-press-1.jpg'),
                require('../img/exercices/chest/incline-chest-press-2.jpg'),
            ],

        },
        
        {
            'id': 'C22',
            'name': 'Incline Dumbbell Flys',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/incline-dumbbell-flys-1.jpg'),
                require('../img/exercices/chest/incline-dumbbell-flys-2.jpg'),
            ],

        },
        {
            'id': 'C23',
            'name': 'Incline Dumbbell Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/incline-dumbbell-press-1.jpg'),
                require('../img/exercices/chest/incline-dumbbell-press-2.jpg'),
            ],

        },
        {
            'id': 'C24',
            'name': 'Incline Pushdown with Cable',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/incline-pushdown-with-cable-1.jpg'),
                require('../img/exercices/chest/incline-pushdown-with-cable-2.jpg'),
            ],

        },
                {
            'id': 'C15',
            'name': 'Low Cable Crossover',
            'level': '1',
            'primaryMuscle': 'Pectoralis major, Anterior deltoid',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Begin with cables at about shoulder height, one in each hand. Take a step forward so that one foot is in front of the other, for stability, and so that there is tension on the cables. Bring hands together in front of you. Try to make your hands overlap (so that the cables cross) a few inches.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/crossover-bands-1.jpg'),
                require('../img/exercices/chest/crossover-bands-2.jpg'),
            ],

        },
        {
            'id': 'C26',
            'name': 'Machine Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/machine-bench-press-1.jpg'),
                require('../img/exercices/chest/machine-bench-press-2.jpg'),
            ],

        },
        {
            'id': 'C27',
            'name': 'Old School Reverse Extensions',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/old-school-reverse-extensions-1.jpg'),
                require('../img/exercices/chest/old-school-reverse-extensions-2.jpg'),
            ],

        },
        {
            'id': 'C28',
            'name': 'One Arm Barbell Floor Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/one-arm-barbell-floor-press-1.jpg'),
                require('../img/exercices/chest/one-arm-barbell-floor-press-2.jpg'),
            ],

        },
        {
            'id': 'C29',
            'name': 'One Arm Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/one-arm-bench-press-1.jpg'),
                require('../img/exercices/chest/one-arm-bench-press-2.jpg'),
            ],

        },
        {
            'id': 'C30',
            'name': 'One Armed Biased Push Up',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/one-armed-biased-push-up-1.jpg'),
                require('../img/exercices/chest/one-armed-biased-push-up-2.jpg'),
            ],

        },
        {
            'id': 'C31',
            'name': 'One Arm Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/one-arm-flat-bench-flys-1.jpg'),
                require('../img/exercices/chest/one-arm-flat-bench-flys-2.jpg'),
            ],

        },
        {
            'id': 'C32',
            'name': 'Pullover Stability Ball Weight',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/pullover-stability-ball-weight-1.jpg'),
                require('../img/exercices/chest/pullover-stability-ball-weight-2.jpg'),
            ],

        },
        {
            'id': 'C33',
            'name': 'Push Up Feet Elevated',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/push-up-feet-elevated-1.jpg'),
                require('../img/exercices/chest/push-up-feet-elevated-2.jpg'),
            ],

        },
        {
            'id': 'C34',
            'name': 'Push Up',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/push-ups-1.jpg'),
                require('../img/exercices/chest/push-ups-2.jpg'),
            ],

        },
                {
            'id': 'C35',
            'name': 'Wide Push Ups',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/push-ups-close-and-wide-hand-versions-1.jpg'),
                require('../img/exercices/chest/push-ups-close-and-wide-hand-versions-2.jpg'),
            ],

        },
                {
            'id': 'C36',
            'name': 'Push Ups Feet on Ball',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/push-ups-with-feet-on-exercise-ball-1.jpg'),
                require('../img/exercices/chest/push-ups-with-feet-on-exercise-ball-2.jpg'),
            ],

        },
                {
            'id': 'C37',
            'name': 'Reverse Bench Press Barbell',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/reverse-triceps-bench-press-with-barbell-1.jpg'),
                require('../img/exercices/chest/reverse-triceps-bench-press-with-barbell-2.jpg'),
            ],

        },
                {
            'id': 'C38',
            'name': 'Smith Machine Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/smith-machine-bench-press-1.jpg'),
                require('../img/exercices/chest/smith-machine-bench-press-2.jpg'),
            ],

        },
                {
            'id': 'C39',
            'name': 'Straight Arm Dumbbell Pullover',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/straight-arm-dumbbell-pullover-1.jpg'),
                require('../img/exercices/chest/straight-arm-dumbbell-pullover-2.jpg'),
            ],

        },
                {
            'id': 'C40',
            'name': 'Wide Grip Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/wide-grip-bench-press-1.jpg'),
                require('../img/exercices/chest/wide-grip-bench-press-2.jpg'),
            ],

        },
                {
            'id': 'C41',
            'name': 'Wide Grip Decline Pullover',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/wide-grip-decline-barbell-pullover-1.jpg'),
                require('../img/exercices/chest/wide-grip-decline-barbell-pullover-2.jpg'),
            ],

        },
                {
            'id': 'C42',
            'name': 'Wide Grip Decline Bench Press',
            'level': '1',
            'primaryMuscle': 'Pectoralis major',
            'secondaryMuscle' : 'Triceps brachii',
            'alternatives' : '',
            'description' : "Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.",
            'group': 'Chest',
            'minRep': '10',
            'maxRep': '15',
            'image': [
                require('../img/exercices/chest/wide-grip-decline-bench-press-1.jpg'),
                require('../img/exercices/chest/wide-grip-decline-bench-press-2.jpg'),
            ],

        },



    ],    
    'back': 
      [{
            'id': 'B1',
            'name': 'Back Body Row',
            'level': '3',
            'primaryMuscle': 'Biceps brachii, Latissimus dorsi',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "With your feet resting on the floor, grab the bar and raise your body until the bottom of the sternum touches the bar. You can vary the exercise by placing your hand towards the front or towards you.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/back/Back_Body_Row_1.jpg'),
                require('../img/exercices/back/Back_Body_Row_2.jpg'),
            ]
        },
        {
            'id': 'B2',
            'name': 'Bent-Over Barbell Deadlift',
            'level': '4',
            'primaryMuscle': 'Latissimus dorsi',
            'secondaryMuscle' : 'Anterior deltoid, Biceps brachii',
            'alternatives' : '',
            'description' : "Grab the barbell with a wide grip (slightly more than shoulder wide) and lean forward. Your upper body is not quite parallel to the floor, but forms a slight angle. The chest's out during the whole exercise. Pull now the barbell with a fast movement towards your belly button, not further up. Go slowly down to the initial position. Don't swing with your body and keep your arms next to your body.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/back/Back_Rear_Deltoid_Row_Barbell_1.jpg'),
                require('../img/exercices/back/Back_Rear_Deltoid_Row_Barbell_2.jpg'),
            ]
        },
        {
            'id': 'B3',
            'name': 'Bent-Over Barbell Deadlift Reverse Grip',
            'level': '4',
            'primaryMuscle': 'Latissimus dorsi',
            'secondaryMuscle' : 'Anterior deltoid, Biceps brachii',
            'alternatives' : '',
            'description' : "The same as regular rowing, but holding a reversed grip (your palms pointing forwards): Grab the barbell with a wide grIp (slightly more than shoulder wide) and lean forward. Your upper body is not quite parallel to the floor, but forms a slight angle. The chest's out during the whole exercise. Pull now the barbell with a fast movement towards your belly button, not further up. Go slowly down to the initial position. Don't swing with your body and keep your arms next to your body.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/back/Back_Reverse_Grips_Bent_Over_Barbell_Rows_1.jpg'),
                require('../img/exercices/back/Back_Reverse_Grips_Bent_Over_Barbell_Rows_2.jpg'),
            ]
        },
        {
            'id': 'B4',
            'name': 'Chin Ups',
            'level': '5',
            'primaryMuscle': 'Biceps brachii, Latissimus dorsi',
            'secondaryMuscle' : 'Trapezius, Anterior Deltoid',
            'alternatives' : '',
            'description' : "Grab the pull up bar with a wide grip, the body is hanging freely. Keep your chest out and pull yourself up till your chin reaches the bar or it touches your neck, if you want to pull behind you. Go with a slow and controlled movement down, always keeping the chest out.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '12',
            'image': [
                require('../img/exercices/back/Back_Chin_Ups_1.jpg'),
                require('../img/exercices/back/Back_Chin_Ups_2.jpg'),
            ]
        },
        {
            'id': 'B5',
            'name': 'Chin Ups Sternum',
            'level': '5',
            'primaryMuscle': 'Biceps brachii, Latissimus dorsi',
            'secondaryMuscle' : 'Trapezius, Anterior Deltoid',
            'alternatives' : '',
            'description' : "Grab the pull up bar with a wide grip, the body is hanging freely. Keep your chest out and pull yourself up till your chin reaches the bar or it touches your neck, if you want to pull behind you. Go with a slow and controlled movement down, always keeping the chest out.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '12',
            'image': [
                require('../img/exercices/back/Back_Gironda_Sternum_Chins_1.jpg'),
                require('../img/exercices/back/Back_Gironda_Sternum_Chins_2.jpg'),
            ]
        },
        {
            'id': 'B6',
            'name': 'Close-Grip Pull-Down',
            'level': '1',
            'primaryMuscle': 'Latissimus dorsi',
            'secondaryMuscle' : 'Biceps brachii',
            'alternatives' : '',
            'description' : "Grip the v-bar with your hands closer than shoulder width apart, with your palms facing each other. Lean back slightly. Pull the bar down towards your chest, keeping your elbows close to your sides as you come down. Pull your shoulders back at the end of the motion.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/back/Back_V_Bar_Pull_Down_1.jpg'),
                require('../img/exercices/back/Back_V_Bar_Pull_Down_2.jpg'),
            ]
        },
        {
            'id': 'B7',
            'name': 'Narrow Parallel Grip Chins',
            'level': '4',
            'primaryMuscle': 'Latissimus dorsi',
            'secondaryMuscle' : 'Anterior deltoid, Biceps brachii',
            'alternatives' : '',
            'description' : "The same as regular rowing, but holding a reversed grip (your palms pointing forwards): Grab the barbell with a wide grIp (slightly more than shoulder wide) and lean forward. Your upper body is not quite parallel to the floor, but forms a slight angle. The chest's out during the whole exercise. Pull now the barbell with a fast movement towards your belly button, not further up. Go slowly down to the initial position. Don't swing with your body and keep your arms next to your body.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/back/narrow-parallel-grip-chin-ups-1.jpg'),
                require('../img/exercices/back/narrow-parallel-grip-chin-ups-2.jpg'),
            ]
        },
        {
            'id': 'B8',
            'name': 'Reverse Cable Flys',
            'level': '1',
            'primaryMuscle': 'Latissimus dorsi, Trapezius',
            'secondaryMuscle' : 'Anterior deltoid, Triceps brachii',
            'alternatives' : '',
            'description' : "Grab the cables so that your arms are stretched and parallel to the ground. Separate the arms 90 degrees towards the ends until they are at the height of your body",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/back/Back_Flys_Cable_1.jpg'),
                require('../img/exercices/back/Back_Flys_Cable_2.jpg'),
            ]
        },


        {
            'id': 'B9',
            'name': 'Seated Cable Row',
            'level': '1',
            'primaryMuscle': 'Latissimus dorsi',
            'secondaryMuscle' : 'Biceps brachii, Brachialis',
            'alternatives' : '',
            'description' : "Sit down, put your feet on the supporting points and grab the bar with a wide grip. Pull the weight with a rapid movement towards your belly button, not upper. Keep your arms and elbows during the movement close to your body. Your shoulders are pulled together. Let the weight slowly down till your arms are completely stretched.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/back/Back_Seated_Cable_Row_1.jpg'),
                require('../img/exercices/back/Back_Seated_Cable_Row_2.jpg'),
            ]
        },
        {
            'id': 'B10',
            'name': 'Single-Arm Dumbbell Row',
            'level': '3',
            'primaryMuscle': 'Latissimus dorsi',
            'secondaryMuscle' : 'Biceps femoris, Biceps brachii, Brachialis',
            'alternatives' : '',
            'description' : "With dumbbells in hand, bend at the hip until hands hang just below the knees (similar to straight-legged-deadlift starting position). Keep upper body angle constant while contracting your lats to pull you ellbows back pinching the shoulder blades at the top. Try not to stand up with every rep, check hands go below knees on every rep.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/back/Back_Rear_Deltoid_Row_Dumbbell_1.jpg'),
                require('../img/exercices/back/Back_Rear_Deltoid_Row_Dumbbell_1.jpg'),
            ]
        },
        {
            'id': 'B11',
            'name': 'Smith Machine Rows',
            'level': '5',
            'primaryMuscle': 'Biceps brachii, Latissimus dorsi',
            'secondaryMuscle' : 'Trapezius, Anterior Deltoid',
            'alternatives' : '',
            'description' : "Grab the pull up bar with a wide grip, the body is hanging freely. Keep your chest out and pull yourself up till your chin reaches the bar or it touches your neck, if you want to pull behind you. Go with a slow and controlled movement down, always keeping the chest out.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '12',
            'image': [
                require('../img/exercices/back/smith-machine-rear-deltoid-row-1.jpg'),
                require('../img/exercices/back/smith-machine-rear-deltoid-row-2.jpg'),
            ]
        },
        {
            'id': 'B12',
            'name': 'Underhand Pull Down',
            'level': '5',
            'primaryMuscle': 'Biceps brachii, Latissimus dorsi',
            'secondaryMuscle' : 'Trapezius, Anterior Deltoid',
            'alternatives' : '',
            'description' : "Grab the pull up bar with a wide grip, the body is hanging freely. Keep your chest out and pull yourself up till your chin reaches the bar or it touches your neck, if you want to pull behind you. Go with a slow and controlled movement down, always keeping the chest out.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '12',
            'image': [
                require('../img/exercices/back/underhand-pull-down-1.jpg'),
                require('../img/exercices/back/underhand-pull-down-2.jpg'),
            ]
        },
        {
            'id': 'B13',
            'name': 'Wide-Grip Pull-Down',
            'level': '2',
            'primaryMuscle': 'Latissimus dorsi',
            'secondaryMuscle' : 'Biceps brachii, Brachialis',
            'alternatives' : '',
            'description' : "Grip the pull-down bar with your hands more open than shoulder width apart, with your palms facing away from you. Lean back slightly. Pull the bar down towards your chest, keeping your elbows close to your sides as you come down. Pull your shoulders back at the end of the motion.",
            'group': 'Back',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/back/Back_Wide_Grip_Lat_Pull_Down_1.jpg'),
                require('../img/exercices/back/Back_Wide_Grip_Lat_Pull_Down_2.jpg'),
            ]
        },
    ],
    'legs': 
      [{
            'id': 'L1',
            'name': 'Barbell Squat',
            'level': '3',
            'primaryMuscle': 'Quadriceps femoris',
            'secondaryMuscle' : 'Gluteus maximus',
            'alternatives' : '',
            'description' : "Stand with feet slightly wider than shoulder-width apart, while standing as tall as you can. Grab a weight plate and hold it out in front of your body with arms straight out. Keep your core tight and stand with a natural arch in your back. Now, push hips back and bend knees down into a squat as far as you can. Hold for a few moments and bring yourself back up to the starting position.",
            'group': 'Legs',
            'minRep': '6',
            'maxRep': '15',
            'image': [
                require('../img/exercices/legs/Legs_Barbell_Squat_1.jpg'),
                require('../img/exercices/legs/Legs_Barbell_Squat_2.jpg'),
            ]
        },
        {
            'id': 'L2',
            'name': 'Bridging',
            'level': '2',
            'primaryMuscle': 'Gluteus maximus',
            'secondaryMuscle' : 'Biceps femoris',
            'alternatives' : '',
            'description' : "Lie on you back with your hips and knees flexed, feet on the ground. From this position, raise your butt off of the ground to a height where your body makes a straight line from your knees to your shoulders. To make the exercise more intense, you can add weight by letting a barbell rest on your hips as you complete the motion, or you can put your feet on a slightly higher surface such as a step or a bench.",
            'group': 'Legs',
            'minRep': '6',
            'maxRep': '15',
            'image': [
                require('../img/exercices/legs/Legs_Bridging_1.jpg'),
                require('../img/exercices/legs/Legs_Bridging_2.jpg'),
            ]
        },
        {
            'id': 'L3',
            'name': 'Dead Lifts',
            'level': '4',
            'primaryMuscle': 'Quadriceps femoris, Latissimus dorsi',
            'secondaryMuscle' : 'Gluteus maximus',
            'alternatives' : '',
            'description' : "Stand firmly, with your feet slightly more than shoulder wide apart. Stand directly behind the bar where it should barely touch your shin, your feet pointing a bit out. Bend down with a straight back, the knees also pointing somewhat out. Grab the bar with a shoulder wide grip, one underhand, one reverse grip. Pull the weight up. At the highest point make a slight hollow back and pull the bar back. Hold 1 or 2 seconds that position. Go down, making sure the back is not bent. Once down you can either go back again as soon as the weights touch the floor, or make a pause, depending on the weight.",
            'group': 'Legs',
            'minRep': '6',
            'maxRep': '15',
            'image': [
                require('../img/exercices/legs/Legs_Barbell_Dead_Lifts_1.jpg'),
                require('../img/exercices/legs/Legs_Barbell_Dead_Lifts_2.jpg'),
            ]
        },
        {
            'id': 'L4',
            'name': 'Good Mornings',
            'level': '5',
            'primaryMuscle': 'Biceps femoris',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "Rest the bar on the trapecius, keep the feet together and bend the upper part of the body down, always keeping the back fully straight and bending the knees slightly. Finally, return to the initial position.",
            'group': 'Legs',
            'minRep': '6',
            'maxRep': '15',
            'image': [
                require('../img/exercices/legs/Legs_Barbell_Good_Mornings_1.jpg'),
                require('../img/exercices/legs/Legs_Barbell_Good_Mornings_2.jpg'),
            ]
        },
        {
            'id': 'L5',
            'name': 'Hyperextensions',
            'level': '3',
            'primaryMuscle': 'Biceps femoris, Gluteus maximus',
            'secondaryMuscle' : 'Erector Spinae',
            'alternatives' : '',
            'description' : "Place yourself on the machine, keep your feet together or slightly apart and bend your upper body down, always keeping your back fully straight. Finally, return to the initial position until the entire body is completely straight",
            'group': 'Legs',
            'minRep': '6',
            'maxRep': '15',
            'image': [
                require('../img/exercices/legs/Legs_Hyperextensions_1.jpg'),
                require('../img/exercices/legs/Legs_Hyperextensions_2.jpg'),
            ]
        },
        {
            'id': 'L6',
            'name': 'Legs Extension',
            'level': '1',
            'primaryMuscle': 'Quadriceps femoris',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "Sitting on the machine, extend the legs completely",
            'group': 'Legs',
            'minRep': '6',
            'maxRep': '15',
            'image': [
                require('../img/exercices/legs/Legs_Legs_Extensions_1.jpg'),
                require('../img/exercices/legs/Legs_Legs_Extensions_2.jpg'),
            ]
        },
        {
            'id': 'L7',
            'name': 'Lunges',
            'level': '4',
            'primaryMuscle': 'Quadriceps femoris',
            'secondaryMuscle' : 'Gluteus maximus',
            'alternatives' : '',
            'description' : "Put barbell on the back of your shoulders. Stand upright, then take the first step forward. Step should bring you forward so that your supporting legs knee can touch the floor. Then stand back up and repeat with the other leg. Remember to keep good posture.",
            'group': 'Legs',
            'minRep': '6',
            'maxRep': '15',
            'image': [
                require('../img/exercices/legs/Legs_Barbell_Lunges_1.jpg'),
                require('../img/exercices/legs/Legs_Barbell_Lunges_2.jpg'),
            ]
        },
        {
            'id': 'L8',
            'name': 'Lying Squat',
            'level': '1',
            'primaryMuscle': 'Quadriceps femoris',
            'secondaryMuscle' : 'Gluteus maximus',
            'alternatives' : '',
            'description' : "Like Squats but without bar and with the machine pads instead",
            'group': 'Legs',
            'minRep': '6',
            'maxRep': '15',
            'image': [
                require('../img/exercices/legs/Legs_Lying_Squat_1.jpg'),
                require('../img/exercices/legs/Legs_Lying_Squat_2.jpg'),
            ]
        },
    ],
    'arms': 
        [{
            'id': 'R1',
            'name': 'Curl Barbell',
            'level': '3',
            'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "Hold the Barbell shoulder-wide, the back is straight, the shoulders slightly back, the arms are streched. Bend the arms, bringing the weight up, with a fast movement. Without pausing, let down the bar with a slow and controlled movement. Don't allow your body to swing during the exercise, all work is done by the biceps, which are the only mucles that should move (pay attention to the elbows).",
            'group': 'Biceps',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/arms/bicep-curls-with-barbell-1.jpg'),
                require('../img/exercices/arms/bicep-curls-with-barbell-2.jpg'),
            ]
          },
          {
              'id': 'R2',
              'name': 'Close Grip EZ Bar Curl',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "Grip the EZ Bar with both hands as close as possible to each other, the back must be straight, the shoulders slightly back, the arms are streched. Bend the arms, bringing the weight up, with a fast movement. Without pausing, let down the bar with a slow and controlled movement. Don't allow your body to swing during the exercise, all work is done by the biceps, which are the only mucles that should move (pay attention to the elbows).",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/close-grip-ez-bar-curl-with-barbell-1.jpg'),
                  require('../img/exercices/arms/close-grip-ez-bar-curl-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R3',
              'name': 'Close Grip Standing Barbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "Grip the Barbell with both hands as close as possible to each other, the back must be straight, the shoulders slightly back, the arms are streched. Bend the arms, bringing the weight up, with a fast movement. Without pausing, let down the bar with a slow and controlled movement. Don't allow your body to swing during the exercise, all work is done by the biceps, which are the only mucles that should move (pay attention to the elbows).",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/close-grip-standing-bicep-curls-with-barbell-1.jpg'),
                  require('../img/exercices/arms/close-grip-standing-bicep-curls-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R4',
              'name': 'Cross Body Hammer Curl',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "Hold two dumbbells and sit on a bench with a straight back, the shoulders are slightly rolled backwards. Your palms point to your body. Bend on arm and bring the weight up with a fast movement. Don't rotate your hands, as with the curls. Without any pause bring the dumbbell down with a slow, controlled movement and repeat the same for the other arm. Don't swing your body during the exercise, the biceps should do all the work here. The elbows are at your side and don't move.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/cross-body-hammer-curl-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/cross-body-hammer-curl-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R5',
              'name': 'Curl Dumbbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "Hold two barbells, the arms are streched, the hands are on your side, the palms face inwards. Bend the arms and bring the weight with a fast movement up. At the same time, rotate your arms by 90 degrees at the very beginning of the movement. At the highest point, rotate a little the weights further outwards. Without a pause, bring them down, slowly. Don't allow your body to swing during the exercise, all work is done by the biceps, which are the only mucles that should move (pay attention to the elbows).",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/biceps-curl-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/biceps-curl-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R6',
              'name': 'Curl Machine',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "Grip the handle with two hands, bend the arms and bring the weight with a fast movement up. You can lift both arms at the same time or separately",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/bicep-curl-with-machine-1.jpg'),
                  require('../img/exercices/arms/bicep-curl-with-machine-2.jpg'),
              ]
          },
          {
              'id': 'R7',
              'name': 'Decline French Press',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
              'secondaryMuscle' : 'None',
              'alternatives' : '',
              'description' : "Hold the SZ-bar or normal barbell and lay down on a flat bench in such a way that around 1/4 of your head is over the edge. Stretch your arms with the bar and bend them so that the bar is lowered. Just before it touches your forehead, push it up. Pay attention to your elbows and arms: only the triceps are doing the work, the rest of the arms should not move.",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/decline-close-grip-bench-to-skull-crusher-1.jpg'),
                  require('../img/exercices/arms/decline-close-grip-bench-to-skull-crusher-2.jpg'),
              ]
          },
          {
              'id': 'R8',
              'name': 'Decline Extensions Dumbbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "Hold the dumbbells and lay down on a flat bench in such a way that around 1/4 of your head is over the edge. Stretch your arms with the dumbbells and bend them so that the dumbbels are lowered. Just before them touches your forehead, push them up. Pay attention to your elbows and arms: only the triceps are doing the work, the rest of the arms should not move. You can do it with both or a single arm at time",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/decline-triceps-extension-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/decline-triceps-extension-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R9',
              'name': 'Drag Curl Barbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "Hold the Barbell shoulder-wide, the back is straight, the shoulders slightly back, the arms are streched and your hands pointing to you. Bend the arms, bringing the weight up, with a fast movement. Without pausing, let down the bar with a slow and controlled movement. Don't allow your body to swing during the exercise, all work is done by the biceps, which are the only mucles that should move (pay attention to the elbows).",
            'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/drag-curl-with-barbell-1.jpg'),
                  require('../img/exercices/arms/drag-curl-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R10',
              'name': 'EZ Bar Curl',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "Hold the EX Bar shoulder-wide, the back is straight, the shoulders slightly back, the arms are streched. Bend the arms, bringing the weight up, with a fast movement. Without pausing, let down the bar with a slow and controlled movement. Don't allow your body to swing during the exercise, all work is done by the biceps, which are the only mucles that should move (pay attention to the elbows).",
            'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/ez-bar-curl-with-barbell-1.jpg'),
                  require('../img/exercices/arms/ez-bar-curl-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R11',
              'name': 'Hammer Curl',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "Hold two dumbbells and sit on a bench with a straight back, the shoulders are slightly rolled backwards. Your pals point to your body. Bend the arms and bring the weight up with a fast movement. Don't rotate your hands, as with the curls. Without any pause bring the dumbbell down with a slow, controlled movement.Don't swing your body during the exercise, the biceps should do all the work here. The elbows are at your side and don't move.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/bicep-hammer-curl-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/bicep-hammer-curl-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R12',
              'name': 'Hammer Curls Cable',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "Take a cable in your hands (palms parallel, point to each other), the body is straight. Bend the arms and bring the weight up with a fast movement. Without any pause bring it back down with a slow, controlled movement, but don't stretch completely your arms. Don't swing your body during the exercise, the biceps should do all the work here. The elbows are at your side and don't move.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/hammer-curls-with-rope-and-cable-1.jpg'),
                  require('../img/exercices/arms/hammer-curls-with-rope-and-cable-2.jpg'),
              ]
          },
          {
              'id': 'R13',
              'name': 'High Cable Curls',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "On a flat bench extend your arms pointing to your head direction. Grip the bar and bend your arms until them are close to your face. Then with a controlled movement bring the arms to the initial position",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/high-cable-curls-1.jpg'),
                  require('../img/exercices/arms/high-cable-curls-2.jpg'),
              ]
          },
          {
              'id': 'R14',
              'name': 'Incline Triceps Extensions Barbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "Grab the bar, stand with your feet shoulder wide, keep your back straight and lean forward a little. Push the bar down, making sure the elbows don't move during the exercise. Without pause go back to the initial position.",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/incline-triceps-extension-with-barbell-1.jpg'),
                  require('../img/exercices/arms/incline-triceps-extension-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R15',
              'name': 'Incline Triceps Extensions Cable',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "Grab the bar, stand with your feet shoulder wide, keep your back straight and lean forward a little. Push the bar down, making sure the elbows don't move during the exercise. Without pause go back to the initial position.",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/incline-triceps-extension-with-cable-1.jpg'),
                  require('../img/exercices/arms/incline-triceps-extension-with-cable-2.jpg'),
              ]
          },
          {
              'id': 'R16',
              'name': 'Incline Triceps Extensions Dumbbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "Grab the dumbbells, stand with your feet shoulder wide, keep your back straight and lean forward a little. Push the dumbbells down, making sure the elbows don't move during the exercise. Without pause go back to the initial position.",
            
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/incline-triceps-extensions-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/incline-triceps-extensions-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R17',
              'name': 'Kneeling Triceps Extensions with Cable',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "",
            
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/kneeling-triceps-extension-with-cable-2-1.jpg'),
                  require('../img/exercices/arms/kneeling-triceps-extension-with-cable-2-2.jpg'),
              ]
          },
          {
              'id': 'R18',
              'name': 'Kneeling Triceps Extensions with Cable',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "",            
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/kneeling-triceps-extension-with-cable-1.jpg'),
                  require('../img/exercices/arms/kneeling-triceps-extension-with-cable-2.jpg'),
              ]
          },
          {
              'id': 'R19',
              'name': 'Low Triceps Extensions with Cable',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "",            
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/low-triceps-extension-with-cable-1.jpg'),
                  require('../img/exercices/arms/low-triceps-extension-with-cable-2.jpg'),
              ]
          },
          {
              'id': 'R20',
              'name': 'Lying Close Grip Biceps With Cable',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/lying-close-grip-biceps-curls-with-cable-1.jpg'),
                  require('../img/exercices/arms/lying-close-grip-biceps-curls-with-cable-2.jpg'),
              ]
          },
          {
              'id': 'R21',
              'name': 'Lying Close Grip Extensions Behind Head with Barbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "",            
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/lying-close-grip-triceps-extension-behind-the-head-with-barbell-1.jpg'),
                  require('../img/exercices/arms/lying-close-grip-triceps-extension-behind-the-head-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R22',
              'name': 'Lying Close Grip Triceps Press to Chin with Barbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "",            
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/lying-close-grip-triceps-press-to-chin-with-barbell-1.jpg'),
                  require('../img/exercices/arms/lying-close-grip-triceps-press-to-chin-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R23',
              'name': 'Lying High Bench Biceps Curl with Barbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/lying-high-bench-biceps-curl-with-barbell-1.jpg'),
                  require('../img/exercices/arms/lying-high-bench-biceps-curl-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R24',
              'name': 'Lying Triceps Extensions Across Face with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "",            
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/lying-triceps-extension-across-face-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/lying-triceps-extension-across-face-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R25',
              'name': 'Lying Triceps Extensions with Cable',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "",            
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/lying-triceps-extension-with-cable-1.jpg'),
                  require('../img/exercices/arms/lying-triceps-extension-with-cable-2.jpg'),
              ]
          },
          {
              'id': 'R26',
              'name': 'Lying Triceps Press with Barbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "",            
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/lying-triceps-press-with-barbell-1.jpg'),
                  require('../img/exercices/arms/lying-triceps-press-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R27',
              'name': 'Lying Two Arm Triceps Extension with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "",            
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/lying-two-arm-triceps-extension-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/lying-two-arm-triceps-extension-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R28',
              'name': 'One Arm Biceps Curl with Olympic Bar or Barbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",
            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/one-arm-bicep-curl-with-olympic-bar-or-barbell-1.jpg'),
                  require('../img/exercices/arms/one-arm-bicep-curl-with-olympic-bar-or-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R29',
              'name': 'One Arm Low Pulley Triceps Extension with Cable',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/one-arm-low-pulley-triceps-extension-with-cable-1.jpg'),
                  require('../img/exercices/arms/one-arm-low-pulley-triceps-extension-with-cable-2.jpg'),
              ]
          },
          {
              'id': 'R30',
              'name': 'One Arm Preacher Curl with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii',
                'secondaryMuscle' : 'Brachioradialis',
                'alternatives' : '',
                'description' : "Hold dumbbell with reverse (or 'overhand') grip, palms facing the floor.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/one-arm-preacher-curl-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/one-arm-preacher-curl-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R31',
              'name': 'Overhead Curl With Cable',
              'level': '3',
              'primaryMuscle': 'Biceps brachii',
                'secondaryMuscle' : 'Brachioradialis',
                'alternatives' : '',
                'description' : "Hold dumbbell with reverse (or 'overhand') grip, palms facing the floor.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/overhead-curl-with-cable-1.jpg'),
                  require('../img/exercices/arms/overhead-curl-with-cable-2.jpg'),
              ]
          },
          {
              'id': 'R32',
              'name': 'Preacher Curl with Barbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii',
                'secondaryMuscle' : 'Brachioradialis',
                'alternatives' : '',
                'description' : "Hold dumbbell with reverse (or 'overhand') grip, palms facing the floor.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/preacher-curl-with-barbell-1.jpg'),
                  require('../img/exercices/arms/preacher-curl-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R33',
              'name': 'Preacher Curl with Cable',
              'level': '3',
              'primaryMuscle': 'Biceps brachii',
                'secondaryMuscle' : 'Brachioradialis',
                'alternatives' : '',
                'description' : "Hold dumbbell with reverse (or 'overhand') grip, palms facing the floor.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/preacher-curl-with-cable-1.jpg'),
                  require('../img/exercices/arms/preacher-curl-with-cable-2.jpg'),
              ]
          },
          {
              'id': 'R34',
              'name': 'Preacher Curl with Machine',
              'level': '3',
              'primaryMuscle': 'Biceps brachii',
                'secondaryMuscle' : 'Brachioradialis',
                'alternatives' : '',
                'description' : "Hold dumbbell with reverse (or 'overhand') grip, palms facing the floor.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/preacher-curl-with-machine-1.jpg'),
                  require('../img/exercices/arms/preacher-curl-with-machine-2.jpg'),
              ]
          },
          {
              'id': 'R35',
              'name': 'Preacher Curl with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii',
                'secondaryMuscle' : 'Brachioradialis',
                'alternatives' : '',
                'description' : "Hold dumbbell with reverse (or 'overhand') grip, palms facing the floor.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/preacher-hammer-curl-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/preacher-hammer-curl-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R36',
              'name': 'Quick Alternating Biceps Curls with Band',
              'level': '3',
              'primaryMuscle': 'Biceps brachii',
                'secondaryMuscle' : 'Brachioradialis',
                'alternatives' : '',
                'description' : "Hold dumbbell with reverse (or 'overhand') grip, palms facing the floor.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/quick-alternating-bicep-curls-with-band-1.jpg'),
                  require('../img/exercices/arms/quick-alternating-bicep-curls-with-band-2.jpg'),
              ]
          },
          {
              'id': 'R37',
              'name': 'Reverse Curl',
              'level': '3',
              'primaryMuscle': 'Biceps brachii',
                'secondaryMuscle' : 'Brachioradialis',
                'alternatives' : '',
                'description' : "Hold dumbbell with reverse (or 'overhand') grip, palms facing the floor.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/biceps-curl-reverse-with-dumbbells-1.jpg'),
                  require('../img/exercices/arms/biceps-curl-reverse-with-dumbbells-2.jpg'),
              ]
          },
          {
              'id': 'R38',
              'name': 'Reverse Grip Triceps Pushdowns',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/reverse-grip-triceps-pushdown-1.jpg'),
                  require('../img/exercices/arms/reverse-grip-triceps-pushdown-2.jpg'),
              ]
          },
          {
              'id': 'R39',
              'name': 'Seated Biceps Curl with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii',
                'secondaryMuscle' : 'Brachioradialis',
                'alternatives' : '',
                'description' : "Hold dumbbell with reverse (or 'overhand') grip, palms facing the floor.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/seated-bicep-curl-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/seated-bicep-curl-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R40',
              'name': 'Seated Inner Biceps Curl with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii',
                'secondaryMuscle' : 'Brachioradialis',
                'alternatives' : '',
                'description' : "Hold dumbbell with reverse (or 'overhand') grip, palms facing the floor.",
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/seated-inner-biceps-curl-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/seated-inner-biceps-curl-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R41',
              'name': 'Seated One Arm Triceps Extensions with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/seated-one-arm-triceps-extension-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/seated-one-arm-triceps-extension-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R42',
              'name': 'Seated Overhead Triceps Extensions with Barbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/seated-overhead-triceps-extension-with-barbell-1.jpg'),
                  require('../img/exercices/arms/seated-overhead-triceps-extension-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R43',
              'name': 'Seated Triceps Press with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/seated-triceps-press-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/seated-triceps-press-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R44',
              'name': 'Single Arm Supinated Triceps Extension with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/single-arm-supinated-triceps-extension-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/single-arm-supinated-triceps-extension-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R45',
              'name': 'Single Arm Triceps Extension with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/single-arm-triceps-extension-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/single-arm-triceps-extension-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R46',
              'name': 'Spider Curl with Barbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/spider-curl-with-barbell-1.jpg'),
                  require('../img/exercices/arms/spider-curl-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R47',
              'name': 'Standing Biceps Curl with Cable',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/standing-biceps-curl-with-cable-1.jpg'),
                  require('../img/exercices/arms/standing-biceps-curl-with-cable-2.jpg'),
              ]
          },
          {
              'id': 'R48',
              'name': 'Standing Inner Biceps Curl With Dumbbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/standing-inner-biceps-curl-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/standing-inner-biceps-curl-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R49',
              'name': 'Standing One Arm Biceps Curl with Cable',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/standing-one-arm-bicep-curl-with-cable-1.jpg'),
                  require('../img/exercices/arms/standing-one-arm-bicep-curl-with-cable-2.jpg'),
              ]
          },
          {
              'id': 'R50',
              'name': 'Standing One Arm Curl Over Incline Bench with Dumbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/standing-one-arm-curl-over-incline-bench-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/standing-one-arm-curl-over-incline-bench-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R51',
              'name': 'Standing One Arm Triceps Extension with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/standing-one-arm-triceps-extension-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/standing-one-arm-triceps-extension-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R52',
              'name': 'Standing Overhead Triceps Extension with Barbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/standing-overhead-triceps-extension-with-barbell-1.jpg'),
                  require('../img/exercices/arms/standing-overhead-triceps-extension-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R53',
              'name': 'Standing Triceps Extension',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/standing-triceps-extension-1.jpg'),
                  require('../img/exercices/arms/standing-triceps-extension-2.jpg'),
              ]
          },
          {
              'id': 'R54',
              'name': 'Standing Triceps Extension with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/standing-triceps-extension-2-1.jpg'),
                  require('../img/exercices/arms/standing-triceps-extension-2-2.jpg'),
              ]
          },
          {
              'id': 'R55',
              'name': 'Straight Arm Push Down',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/straight-arm-push-down-1.jpg'),
                  require('../img/exercices/arms/straight-arm-push-down-2.jpg'),
              ]
          },
          {
              'id': 'R56',
              'name': 'Tate Press',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/tate-press-1.jpg'),
                  require('../img/exercices/arms/tate-press-2.jpg'),
              ]
          },
          {
              'id': 'R57',
              'name': 'Triceps Dips',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/tricep-dips-1.jpg'),
                  require('../img/exercices/arms/tricep-dips-2.jpg'),
              ]
          },
          {
              'id': 'R58',
              'name': 'Triceps Dips Body Weight',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/tricep-dips-using-body-weight-1.jpg'),
                  require('../img/exercices/arms/tricep-dips-using-body-weight-2.jpg'),
              ]
          },
          {
              'id': 'R59',
              'name': 'Triceps Extensions with Machine',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/triceps-extensions-using-machine-1.jpg'),
                  require('../img/exercices/arms/triceps-extensions-using-machine-2.jpg'),
              ]
          },
          {
              'id': 'R60',
              'name': 'Triceps Pushdowns with Cable',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/triceps-pushdown-with-cable-1.jpg'),
                  require('../img/exercices/arms/triceps-pushdown-with-cable-2.jpg'),
              ]
          },
          {
              'id': 'R61',
              'name': 'Triceps Pushdowns with Rope and Cable',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/triceps-pushdown-with-rope-and-cable-1.jpg'),
                  require('../img/exercices/arms/triceps-pushdown-with-rope-and-cable-2.jpg'),
              ]
          },
          {
              'id': 'R62',
              'name': 'Triceps Push with V Bar and Cable',
              'level': '3',
              'primaryMuscle': 'Triceps brachii',
            'secondaryMuscle' : 'None',
                'alternatives' : '',
                'description' : "",
              'group': 'Triceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/triceps-pushdown-with-v-bar-and-cable-1.jpg'),
                  require('../img/exercices/arms/triceps-pushdown-with-v-bar-and-cable-2.jpg'),
              ]
          },
          {
              'id': 'R63',
              'name': 'Two Arm Preacher Curl with Dumbbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/two-arm-preacher-curl-with-dumbbell-1.jpg'),
                  require('../img/exercices/arms/two-arm-preacher-curl-with-dumbbell-2.jpg'),
              ]
          },
          {
              'id': 'R64',
              'name': 'Wide Grip Standing Biceps Curl with Barbell',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/wide-grip-standing-biceps-curl-with-barbell-1.jpg'),
                  require('../img/exercices/arms/wide-grip-standing-biceps-curl-with-barbell-2.jpg'),
              ]
          },
          {
              'id': 'R65',
              'name': 'Zottman Curl with Dumbbells',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/zottman-curl-with-dumbbells-1.jpg'),
                  require('../img/exercices/arms/zottman-curl-with-dumbbells-2.jpg'),
              ]
          },
          {
              'id': 'R66',
              'name': 'Zottman Preacher Curl with Dumbbells',
              'level': '3',
              'primaryMuscle': 'Biceps brachii, Brachialis',
            'secondaryMuscle' : 'Brachioradialis',
            'alternatives' : '',
            'description' : "",            
              'group': 'Biceps',
              'minRep': '8',
              'maxRep': '15',
              'image': [
                  require('../img/exercices/arms/zottman-preacher-curl-with-dumbbells-1.jpg'),
                  require('../img/exercices/arms/zottman-preacher-curl-with-dumbbells-2.jpg'),
              ]
          },
    ],
    'shoulders': 
      [{
            'id': 'S1',
            'name': 'Arnold Press',
            'level': '1',
            'primaryMuscle': 'Anterior deltoid',
            'secondaryMuscle' : 'Triceps brachii, Trazpezius',
            'alternatives' : '',
            'description' : "Sit on a bench, the back rest should be almost vertical. Take two dumbbells and bring them up to shoulder height, the palms and the elbows point during the whole exercise to the front. Press the weights up, at the highest point they come very near but don't touch. Go slowly down and repeat.",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/arnold-press-1.jpg'),
                require('../img/exercices/shoulders/arnold-press-2.jpg'),
            ]
        },
        {
            'id': 'S2',
            'name': 'Barbell Shoulder Press',
            'level': '1',
            'primaryMuscle': 'Anterior deltoid',
            'secondaryMuscle' : 'Triceps brachii, Trazpezius',
            'alternatives' : '',
            'description' : "Sit on a bench, the back rest should be almost vertical. Take two dumbbells and bring them up to shoulder height, the palms and the elbows point during the whole exercise to the front. Press the weights up, at the highest point they come very near but don't touch. Go slowly down and repeat.",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/barbell-shoulder-press-1.jpg'),
                require('../img/exercices/shoulders/barbell-shoulder-press-2.jpg'),
            ]
        },
        {
            'id': 'S3',
            'name': 'Barbell Shrugs',
            'level': '2',
            'primaryMuscle': 'Anterior deltoid, Trapezius',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "Take a barbell and stand with a straight body, the arms are hanging freely in front of you. Lift from this position the shoulders as high as you can, but don't bend the arms during the movement. On the highest point, make a short pause of 1 or 2 seconds before returning slowly to the initial position. When training with a higher weight, make sure that you still do the whole movement!",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/barbell-shrugs-1.jpg'),
                require('../img/exercices/shoulders/barbell-shrugs-2.jpg'),
            ]
        },
        {
            'id': 'S4',
            'name': 'Bent Over Lateral Cable Raises',
            'level': '4',
            'primaryMuscle': 'Anterior deltoid',
            'secondaryMuscle' : 'Trapezius',
            'alternatives' : '',
            'description' : "Bend forward as far as possible, with arms slightly bent at the elbow. Perform a lateral raise while maintaining the bend in your elbow.",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/bent-over-lateral-cable-raises-1.jpg'),
                require('../img/exercices/shoulders/bent-over-lateral-cable-raises-2.jpg'),
            ]
        },
        {
            'id': 'S5',
            'name': 'Bent Over Rear Deltoid Raises',
            'level': '4',
            'primaryMuscle': 'Anterior deltoid',
            'secondaryMuscle' : 'Trapezius',
            'alternatives' : '',
            'description' : "Bend forward as far as possible, with arms slightly bent at the elbow. Perform a lateral raise while maintaining the bend in your elbow.",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/bent-over-rear-deltoid-raise-with-head-on-bench-1.jpg'),
                require('../img/exercices/shoulders/bent-over-rear-deltoid-raise-with-head-on-bench-1.jpg'),
            ]
        },
        {
            'id': 'S6',
            'name': 'Cable SHoulder Shrugs',
            'level': '2',
            'primaryMuscle': 'Anterior deltoid, Trapezius',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "Take the bar and stand with a straight body, the arms are hanging freely in front of you. Lift from this position the shoulders as high as you can, but don't bend the arms during the movement. On the highest point, make a short pause of 1 or 2 seconds before returning slowly to the initial position. When training with a higher weight, make sure that you still do the whole movement!",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/cable-shoulder-shrugs-1.jpg'),
                require('../img/exercices/shoulders/cable-shoulder-shrugs-2.jpg'),
            ]
        },
        {
            'id': 'S7',
            'name': 'Dumbbell Raises',
            'level': '2',
            'primaryMuscle': 'Trazpezius, Anterior deltoid',
            'secondaryMuscle' : 'None',
            'alternatives' : '',
            'description' : "hold the dumbbells near your pelvis. Raise your elbows until the dumbbells are over your chest and the elbows at the same height of your shoulders",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/dumbbell-raise-1.jpg'),
                require('../img/exercices/shoulders/dumbbell-raise-2.jpg'),
            ]
        },
        {
            'id': 'S8',
            'name': 'Dumbbell Shoulder Press',
            'level': '1',
            'primaryMuscle': 'Anterior deltoid',
            'secondaryMuscle' : 'Trazpezius',
            'alternatives' : '',
            'description' : "Sit on a bench, the back rest should be almost vertical. Take two dumbbells and bring them up to shoulder height, the palms and the elbows point during the whole exercise to the front. Press the weights up, at the highest point they come very near but don't touch. Go slowly down and repeat.",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/dumbbell-shoulder-press-1.jpg'),
                require('../img/exercices/shoulders/dumbbell-shoulder-press-2.jpg'),
            ]
        },
        {
            'id': 'S9',
            'name': 'Front Barbell Raises',
            'level': '2',
            'primaryMuscle': 'Anterior deltoid',
            'secondaryMuscle' : 'Trazpezius',
            'alternatives' : '',
            'description' : "Bring the arms up in front of the body to eye level and with only a slight bend in the elbow.",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/front-barbell-raises-1.jpg'),
                require('../img/exercices/shoulders/front-barbell-raises-2.jpg'),
            ]
        },
        {
            'id': 'S10',
            'name': 'Front Cable Raises',
            'level': '2',
            'primaryMuscle': 'Anterior deltoid',
            'secondaryMuscle' : 'Trazpezius',
            'alternatives' : '',
            'description' : "Bring the arm up in front of the body to eye level and with only a slight bend in the elbow. You can performance this exercise with one or both arms at the same time",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/front-cable-raises-1.jpg'),
                require('../img/exercices/shoulders/front-cable-raises-2.jpg'),
            ]
        },
        {
            'id': 'S11',
            'name': 'Front Dumbbell Raise',
            'level': '2',
            'primaryMuscle': 'Anterior deltoid',
            'secondaryMuscle' : 'Trazpezius',
            'alternatives' : '',
            'description' : "Bring the arm up in front of the body to eye level and with only a slight bend in the elbow. You can performance this exercise with one or both arms at the same time",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/front-dumbbell-raise-1.jpg'),
                require('../img/exercices/shoulders/front-dumbbell-raise-2.jpg'),
            ]
        },
        {
            'id': 'S12',
            'name': 'Front Raises one hand',
            'level': '1',
            'primaryMuscle': 'Anterior deltoid',
            'secondaryMuscle' : 'Trazpezius',
            'alternatives' : '',
            'description' : "Bring the arm up in front of the body to eye level and with only a slight bend in the elbow. You can performance this exercise with one or both arms at the same time",
            'group': 'Shoulders',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/shoulders/front-raises-1.jpg'),
                require('../img/exercices/shoulders/front-raises-2.jpg'),
            ]
        },
    ],
    'abs':
        [{
            'id': 'A1',
            'name': 'Ab Rollout on knees Barbell',
            'level': '5',
            'primaryMuscle': 'Rectus abdominis, Transverse Abdominis',
            'secondaryMuscle' : 'Obliques, Transverse Abdominis, Latissimus dorsi',
            'alternatives' : '',
            'description' : '',
            'group': 'Abs',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/abs/ab-rollout-on-knees-with-barbell-1.jpg'),
                require('../img/exercices/abs/ab-rollout-on-knees-with-barbell-2.jpg'),
            ]
        },
        {
            'id': 'A2',
            'name': 'Air Bike',
            'level': '2',
            'primaryMuscle': 'Rectus abdominis',
            'secondaryMuscle' : 'Obliques, Transverse Abdominis',
            'alternatives' : '',
            'description' : '',
            'group': 'Abs',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/abs/air-bike-1.jpg'),
                require('../img/exercices/abs/air-bike-2.jpg'),
            ]
        },
        {
            'id': 'A3',
            'name': 'Bent Knee Hip Raise',
            'level': '2',
            'primaryMuscle': 'Rectus abdominis',
            'secondaryMuscle' : 'Obliques, Transverse Abdominis',
            'alternatives' : '',
            'description' : '',
            'group': 'Abs',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/abs/bent-knee-hip-raise-1.jpg'),
                require('../img/exercices/abs/bent-knee-hip-raise-2.jpg'),
            ]
        },
        {
            'id': 'A4',
            'name': 'Crunches on Bench',
            'level': '3',
            'primaryMuscle': 'Rectus abdominis',
            'secondaryMuscle' : 'Obliques, Transverse Abdominis,',
            'alternatives' : '',
            'description' : '',
            'group': 'Abs',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/abs/crunches-1.jpg'),
                require('../img/exercices/abs/crunches-2.jpg'),
            ]
        },
        {
            'id': 'A5',
            'name': 'Crunches on Ball',
            'level': '4',
            'primaryMuscle': 'Rectus abdominis',
            'secondaryMuscle' : 'Obliques, Transverse Abdominis,',
            'alternatives' : '',
            'description' : '',
            'group': 'Abs',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/abs/crunches-with-legs-on-stability-ball-1.jpg'),
                require('../img/exercices/abs/crunches-with-legs-on-stability-ball-2.jpg'),
            ]
        },
        {
            'id': 'A6',
            'name': 'Flat Leg Raises',
            'level': '3',
            'primaryMuscle': 'Rectus abdominis, Obliques',
            'secondaryMuscle' : 'Transverse Abdominis,',
            'alternatives' : '',
            'description' : '',
            'group': 'Abs',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/abs/flat-bench-leg-raises-1.jpg'),
                require('../img/exercices/abs/flat-bench-leg-raises-2.jpg'),
            ]
        },
        {
            'id': 'A7',
            'name': 'Seated Ab Crunch Cable',
            'level': '4',
            'primaryMuscle': 'Rectus abdominis',
            'secondaryMuscle' : 'Obliques, Transverse Abdominis,',
            'alternatives' : '',
            'description' : '',
            'group': 'Abs',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/abs/seated-ab-crunch-with-cable-1.jpg'),
                require('../img/exercices/abs/seated-ab-crunch-with-cable-2.jpg'),
            ]
        },
        {
            'id': 'A8',
            'name': 'Side Bend Dumbbell',
            'level': '2',
            'primaryMuscle': 'Rectus abdominis',
            'secondaryMuscle' : 'Obliques, Transverse Abdominis,',
            'alternatives' : '',
            'description' : '',
            'group': 'Abs',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/abs/side-bend-with-dumbbell-1.jpg'),
                require('../img/exercices/abs/side-bend-with-dumbbell-2.jpg'),
            ]
        },
        {
            'id': 'A9',
            'name': 'Side Plank',
            'level': '4',
            'primaryMuscle': 'Obliques, Transverse Abdominis',
            'secondaryMuscle' : 'Rectus abdominis',
            'alternatives' : '',
            'description' : '',
            'group': 'Abs',
            'minRep': '8',
            'maxRep': '15',
            'image': [
                require('../img/exercices/abs/side-plank-1.jpg'),
                require('../img/exercices/abs/side-plank-2.jpg'),
            ]
        },
    ]
}



class API_EXTRACTOR {
    getExercices() {
        const query = API
        // const data = query
        return query
    }
    getChestExercices() {
        const query = API.chest
        return query
    }

    getBackExercices() {
        const query = API.back
        return query
    }
    getLegExercices() {
        const query = API.legs
        return query
    }
    getArmsExercices() {
        const query = API.arms
        return query
    }
    getShouldersExercices() {
        const query = API.shoulders
        return query
    }
    getAbsExercices() {
        const query = API.abs
        return query
    }

    getExerciceWithId(id) {
        let query = ''
        const MUSCLE = id.charAt(0)
        const ID = parseInt(id.substr(1)) - 1

        switch (MUSCLE) {
            case 'A':
                query = API.abs[ID]
                break
            case 'B':
                query = API.back[ID]
                break
            case 'C':
                query = API.chest[ID]
                break
            case 'L':
                query = API.legs[ID]
                break
            case 'R':
                query = API.arms[ID]
                break
            case 'S':
                query = API.shoulders[ID]
                break
        }
        return query
    }
}


export default new API_EXTRACTOR()

/*


Dumbbell Concentration Curl
Brachialis Biceps brachii
Sit on bench. Grasp dumbbell between feet. Place back of upper arm to inner thigh. Lean into leg to raise elbow slightly.

Barbell Triceps Extension
Anterior deltoid Pectoralis major
Triceps brachii
Lower forearm behind upper arm with elbows remaining overhead. Extend forearm overhead. Lower and repeat.

Bench Press Narrow Grip
Anterior deltoid Pectoralis major
Triceps brachii
Lay down on a bench, the bar is directly over your eyes, the knees form a slight angle and the feet are firmly on the ground. Hold the bar with a narrow grip (around 20cm.). Lead the weight slowly down till the arms are parallel to the floor (elbow: right angle), press then the bar up. When bringing the bar down, don't let it down on your nipples as with the regular bench pressing, but somewhat lower.

Biceps Curl With Cable
Biceps brachii
Stand around 30 - 40cm away from the cable, the feet are firmly on the floor. Take the bar and lift the weight with a fast movements. Lower the weight as with the dumbbell curls slowly and controlled.

Dips Between Two Benches
Triceps brachii
Put two benches so far appart, that you can hold onto one with your hands and are just able to reach the other with your feet. The legs stay during the exercise completely stretched. With your elbows facing back, bend them as much as you can. Push yourself up, but don't stretch out the arms.
https://wger.de/es/exercise/83/view/dips-between-two-benches

French Press (skullcrusher) Dumbbells
Triceps brachii
Hold the dumbbells and lay down on a flat bench in such a way that around 1/4 of your head is over the edge. Stretch your arms with the weights and bend them so that the dumbbells are lowered (make sure they don't touch each other). Just before they touch your forehead, push them up.
Pay attention to your elbows and arms: only the triceps are doing the work, the rest of the arms should not move.
https://wger.de/es/exercise/85/view/french-press-skullcrusher-dumbbells


Preacher Curls
Brachialis Biceps brachii
Place the EZ curl bar on the rest handles in front of the preacher bench. Lean over the bench and grab the EZ curl bar with palms up. Sit down on the preacher bench seat so your upper arms rest on top of the pad and your chest is pressed against the pad. Lower the weight until your elbows are extended and arms are straight. Bring the weights back up to the starting point by contracting biceps. Repeat
https://wger.de/es/exercise/193/view/preacher-curls

Push Ups
Triceps brachii
Anterior deltoid Pectoralis major Rectus abdominis
Start with your body streched, your hands are shoulder-wide appart on the ground. Push yourself off the ground till you strech your arms. The back is always straight and as well as the neck (always look to the ground). Lower yourself to the initial position and repeat.
https://wger.de/es/exercise/195/view/push-ups

Reverse Bar Curl
Biceps brachii
Hold bar with reverse (or "overhand") grip, palms facing the floor.

Tricep Dumbbell Kickback
Triceps brachii
Start with a dumbbell in each hand and your palms facing your torso. Keep your back straight with a slight bend in the knees and bend forward at the waist. Your torso should be almost parallel to the floor. Make sure to keep your head up. Your upper arms should be close to your torso and parallel to the floor. Your forearms should be pointed towards the floor as you hold the weights. There should be a 90-degree angle formed between your forearm and upper arm. This is your starting position.
Now, while keeping your upper arms stationary, exhale and use your triceps to lift the weights until the arm is fully extended. Focus on moving the forearm.
After a brief pause at the top contraction, inhale and slowly lower the dumbbells back down to the starting position.
Repeat the movement for the prescribed amount of repetitions.






































































































































































































































































































































*/