import React, { Component } from 'react'
import { AsyncStorage, View, Text, TouchableOpacity, Dimensions, ScrollView, FlatList, StyleSheet, Image, ImageBackground, StatusBar, RefreshControl} from 'react-native'
import { createStackNavigator } from 'react-navigation'
import { withNavigation } from 'react-navigation';




class LangView extends Component{

	state = {
			language: true
	}

	componentWillMount = async () => {
		try{
			const LANG = await AsyncStorage.getItem('@Pump:LANGUAGE');
			const { navigate } = this.props.navigation;
			console.log('LANG', LANG)
			if (LANG != null) {
				navigate('Home')
			}else{
				this.setState({language: false})
			}
		
		}catch(error) {
			setTimeout(() => {
				this.setState({language: false})
			})
		}
	}


	setLanguage = async (lang) => {
		try{
			await AsyncStorage.setItem('@Pump:LANGUAGE', lang)
			console.log('from lang', lang)
			const { navigate } = this.props.navigation;
			setTimeout(() => {
				navigate('Home')

			},1000)
		}catch(error){
			console.log('Error al seleccionar el lenguaje')
		}
	}




	render() {
		return (
			<View style = {{width: '100%', height: '100%'}}>					

					{!this.state.language ? 

						<View style = {styles.container}>
							<StatusBar barStyle = 'light-content' />
							<View style = {{width: '100%', alignItems: 'center', marginTop: '-20%', height: '30%'}}>
								<Image style = {{width: '100%'}} source = {require('../../../img/logos/pumpLogo.png')}
								resizeMode = 'contain' />
							</View>					
							<View style= {{alignItems: 'center'}}>
								<View style = {{marginBottom: 10}}>
									<Text style = {{fontSize: 24, color: 'white'}} >Selecciona tu lenguaje</Text>
								</View>
								<TouchableOpacity activeOpacity = {0.8} style = {[{marginBottom: 30},styles.imageBox]}
								onPress = {() => this.setLanguage('ES')}>
									<Image style = {styles.image} source = {require('../../../img/icons/lang/es.jpg')}/>
								</TouchableOpacity>
								<View style = {{marginBottom: 10}}>
									<Text style = {{fontSize: 24, color: 'white'}} >Select your language</Text>
								</View>
								<TouchableOpacity activeOpacity = {0.8} style = {styles.imageBox}
								onPress = {() => this.setLanguage('EN')}>
									<Image style = {styles.image} source = {require('../../../img/icons/lang/en.jpg')}/>
								</TouchableOpacity>
							</View>
							<View style= {{width: '100%', height: '0%'}}></View>
						</View>	

						:

						null
					}
				
					</View>
				
				
			)
	}



}

export default LangView

const styles = StyleSheet.create({
	container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'black',
        // paddingVertical: '20%'

    },
    imageBox: {
    	shadowColor: '#000000',
        shadowOpacity: 0.6,
        shadowRadius: 3,
        shadowOffset: {
            width: 2,
            height: 2
        },
        borderRadius: 10,
    },
    image: {
        borderRadius: 10,

    }
})