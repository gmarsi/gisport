import React, { Component } from 'react'
import { View, Text, Image, StyleSheet,StatusBar,Animated, Keyboard, Dimensions, TouchableOpacity, AsyncStorage, TextInput, ScrollView, ImageBackground, Switch, } from 'react-native'
import { ImagePicker } from 'expo';
import { createStackNavigator } from 'react-navigation'
import SideMenu from 'react-native-side-menu';
import Menu from '../../components/menu/Menu';

var USER_DATA = []
const window = Dimensions.get('window');
const IMAGE_HEIGHT = window.width / 4;
const IMAGE_HEIGHT_SMALL = window.width /7;

class ProfileRow extends Component {
    constructor(props) {
        super(props)
        this.state = {
                editable: this.props.editable
            }
    }

	render() {
		return(
			<View style = {{alignItems: 'flex-start', width: '100%', marginBottom: 15,}}>             
				<Text style = {{fontSize: 16, color: '#fff'}}>{this.props.title}</Text>
				<View style = {{width: '100%', height: 50,   marginTop: 5, backgroundColor: 'rgba(255,255,255,0.4)'}}>
					<TextInput
				        style={styles.input}
				        onChangeText={this.props.onChange}
				        value={this.props.value}
				        editable = {this.props.editable}
						underlineColorAndroid='transparent'
                        onFocus = {this.props.onFocus}
                        clearButtonMode = 'always'
                        returnKeyType = 'done'
                        keyboardType = {this.props.keyboardType || 'default'}


				      />
				</View>                
			</View>
		)
	}
}


export default class ProfileScreen extends Component {

	constructor(props) {
		super(props)
		this.state = { 
			USER_NAME: '',
			USER_LASTNAME: '',
			USER_USERNAME :'',
			USER_EMAIL: '',
			USER_PHONE: '',
			USER_GENDER :'',
            temp_user_name: '',
            temp_user_lastname: '',
            temp_user_username :'',
            temp_user_email: '',
            temp_user_phone: '',
            temp_user_gender :'',
			edit: false,
			image: null,
			isOpen: false,
            initialValues: [],
            genderMale: false,
            genderFemale: false
		}
		this.toggle = this.toggle.bind(this);
        this.keyboardHeight = new Animated.Value(0);
        this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
	}


    componentWillMount = async () => {
        this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
      try {
        const USER_NAME = await AsyncStorage.getItem('@Pump:USER_NAME');
        const USER_LASTNAME = await AsyncStorage.getItem('@Pump:USER_LASTNAME');
        const USER_USERNAME = await AsyncStorage.getItem('@Pump:USER_USERNAME');
        const USER_EMAIL = await AsyncStorage.getItem('@Pump:USER_EMAIL');
        const USER_PHONE = await AsyncStorage.getItem('@Pump:USER_PHONE');
        const USER_GENDER = await AsyncStorage.getItem('@Pump:USER_GENDER');

        // main values
        this.setState({
            USER_NAME: USER_NAME,
            USER_LASTNAME: USER_LASTNAME,
            USER_USERNAME: USER_USERNAME,
            USER_EMAIL: USER_EMAIL,
            USER_PHONE: USER_PHONE,
            USER_GENDER: USER_GENDER });

        //aux values
        this.setState({
            temp_user_name: USER_NAME, 
            temp_user_lastname: USER_LASTNAME,
            temp_user_username: USER_USERNAME,
            temp_user_email: USER_EMAIL,
            temp_user_phone: USER_PHONE,
            temp_user_gender: USER_GENDER });

           
        
        if(USER_GENDER) {
            if(USER_GENDER === 'male') {
                this.setState({genderMale: true})
            }else {
                this.setState({genderFemale: true})
            }
        }



      } catch (error) {
        console.log('ERROR SETTING ITEM: ', error)
      }
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    _storeData = async () => {
      try {
        await AsyncStorage.setItem('@Pump:USER_NAME', this.state.USER_NAME || '');
        await AsyncStorage.setItem('@Pump:USER_LASTNAME', this.state.USER_LASTNAME || '');
        await AsyncStorage.setItem('@Pump:USER_USERNAME', this.state.USER_USERNAME || '');
        await AsyncStorage.setItem('@Pump:USER_EMAIL', this.state.USER_EMAIL || '');
        await AsyncStorage.setItem('@Pump:USER_PHONE', this.state.USER_PHONE || '');
        await AsyncStorage.setItem('@Pump:USER_GENDER', this.state.USER_GENDER || '');
        this.editMode(0)

      } catch (error) {
        console.log('ERROR SETTING ITEM: ', error)
      }
    }
    reestablishInitialValues() {
        this.setState({
            USER_NAME: this.state.temp_user_name,
            USER_LASTNAME: this.state.temp_user_lastname,
            USER_USERNAME: this.state.temp_user_username,
            USER_EMAIL: this.state.temp_user_email,
            USER_PHONE: this.state.temp_user_phone,
            USER_GENDER: this.state.temp_user_gender
        })        
        console.log('initial values', this.state.USER_GENDER)
        if(this.state.USER_GENDER == 'male') {
            this.setState({genderMale: true, genderFemale: false})
        }else {
            this.setState({genderMale: false, genderFemale: true})
        }
    }

    updateInputValue(value, field) {
        switch(field){
            case 'name' : this.setState({USER_NAME: value})
                break;
            case 'lastname' :  this.setState({USER_LASTNAME: value})
                break;
            case 'username' : this.setState({USER_USERNAME: value})
                break;
            case 'email' : this.setState({USER_EMAIL: value})
                break;
            case 'phone' : this.setState({USER_PHONE: value})
                break;
            
        }
    }

	editMode() {
        this.setState({edit: !this.state.edit})
	}

    cancelEdit() {
        this.setState({edit: !this.state.edit})
        this.reestablishInitialValues()
    }

	ImagePicker() {
		alert('Image picker')
	}

	toggle() {
        this.setState({
          isOpen: !this.state.isOpen,
        });
    } 

    updateMenuState(isOpen) {
        this.setState({ isOpen });
    }

    onMenuItemSelected(item) {
        this.setState({
          isOpen: false,
          selectedItem: item,
        })
         
        const { navigate } = this.props.navigation;
        switch (item)
        {
        case "Profile": navigate('Profile')
            break;
        case 'Health': alert('Health')
            break;
        case 'Training': alert('Training')
            break;
        case 'Routines': navigate('MyRoutines')
            break;
        case 'Timer': navigate('Timer')
            break;
        case 'Settings': alert('Settings')
            break;
        case 'Exercices': navigate('Exercices')
            break;
        case 'About': navigate('About')
            break;
        default: 
            navigate('Home')
        }

    }

    changeGender(gender) {
            console.log('gender is', gender)

        switch(gender) {
            case 'male' : 
                this.setState({USER_GENDER: gender})
                this.setState({genderMale: true, genderFemale: false}) 
                break;
            case 'female' : 
                this.setState({USER_GENDER: gender}) 
                this.setState({genderMale: false, genderFemale: true})
                break;
        }
    }

    keyboardWillShow = (event) => {
        Animated.parallel([
          Animated.timing(this.keyboardHeight, {
            duration: 100,
            toValue: event.endCoordinates.height + 20,
          }),
          Animated.timing(this.imageHeight, {
            duration: event.duration,
            toValue: IMAGE_HEIGHT_SMALL,
          }),
        ]).start();
      };

      keyboardWillHide = (event) => {
        Animated.parallel([
          Animated.timing(this.keyboardHeight, {
            duration: event.duration,
            toValue: 0,
          }),
          Animated.timing(this.imageHeight, {
            duration: event.duration,
            toValue: IMAGE_HEIGHT,
          }),
        ]).start();
      };


	render() {
		const menu = <Menu onItemSelected={(item) => this.onMenuItemSelected(item)} />;
		return (
			<SideMenu
                menu={menu}
                isOpen={this.state.isOpen}
                onChange={isOpen => this.updateMenuState(isOpen)} 
                disableGestures = {true} >
                <ImageBackground source = {require('../../../img/prozis.png')} style={{width: '100%', height: '100%'}}>
                    <View style={{paddingTop: 20}}>
                    <StatusBar barStyle = 'light-content' />
        			{this.state.edit ? <HeaderOnEdit cancelEdit = {() => this.cancelEdit(1)} saveEdit = {() => this._storeData()}/> : null }
                    {!this.state.edit ? <HeaderOnNoEdit toggleMenu = {() => this.toggle()} edit = {() => this.editMode(0)} /> :null } 
                    </View>
                        <Animated.View style={[styles.container, { paddingBottom: this.keyboardHeight }]}>
                            <ScrollView style = {{width: '100%'}}  ref='_scrollView'>
        					{/*<View style = {{width: '100%', height: 150, marginBottom: 10,  alignItems: 'center', justifyContent: 'center'}}>
        						<View style = {{height: 100, width: 100, borderRadius: 50, borderWidth: 2, borderColor: '#fff', alignItems: 'center', justifyContent: 'center'}}>
                                    <Image source = {require('../../../img/bkgr.png')} style = {{width: '100%', height: '100%', borderRadius: 50}} />
                                </View>
        						<Text onPress = {() => this.ImagePicker()} style = {{fontSize: 16, fontWeight: 'bold', color: '#fff', width: '100%', textAlign: 'center', marginTop: 15}}>Change Profile Photo</Text>
        					</View>*/}
        					<View style = {{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
        						<ProfileRow editable = {this.state.edit} title = {'Name'} value = {this.state.USER_NAME} onChange = {(text) => this.updateInputValue(text, 'name')}/>
        						<ProfileRow editable = {this.state.edit} title = {'Lastname'} value = {this.state.USER_LASTNAME} onChange = {(text) => this.updateInputValue(text, 'lastname')} />
        						<ProfileRow editable = {this.state.edit} title = {'Username'} value = {this.state.USER_USERNAME} onChange = {(text) => this.updateInputValue(text, 'username')} />	
        					</View>
        					<View style = {{marginTop: 50, marginBottom: 30}}>
        						<Text style = {{fontSize: 20, fontWeight: 'bold', color: '#fff'}}> 
        							Private Information
        						</Text>
        					</View>
        					<View style = {{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
        						<ProfileRow keyboardType = 'email-address' editable = {this.state.edit} title = {'Email address'} value = {this.state.USER_EMAIL} onChange = {(text) => this.updateInputValue(text, 'email')} />
        						<ProfileRow keyboardType = 'phone-pad'editable = {this.state.edit} title = {'Phone'} value = {this.state.USER_PHONE} onChange = {(text) => this.updateInputValue(text, 'phone')} />
        						<View style = {{alignItems: 'flex-start', width: '100%', marginBottom: 15,}}>             
                                    <Text style = {{fontSize: 16, color: '#fff'}}>Gender</Text>
                                    <View style = {{width: '100%', height: 50,   marginTop: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                        <View style = {{width: '50%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                                            <Switch value = {this.state.genderMale} disabled = {!this.state.edit} onValueChange = {() => this.changeGender('male')} /><Text style = {{marginLeft: 5, color: '#fff', fontSize: 20}}>Male</Text>
                                        </View>
                                        <View style = {{width: '50%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                                            <Switch value = {this.state.genderFemale} disabled = {!this.state.edit} onValueChange = {() => this.changeGender('female')} /><Text style = {{marginLeft: 5, color: '#fff', fontSize: 20}}>Female</Text>
                                        </View>                                        
                                        
                                    </View>                
                                </View>	
        					</View>
                            </ScrollView>
                        </Animated.View>
                </ImageBackground>
			</SideMenu>
		)
	}
}

class HeaderOnEdit extends Component {
    render(){
        return (
            <View style = {styles.headers}>
                <Text style = {{fontSize: 20, color: '#fff'}} onPress = {this.props.cancelEdit}>Cancel</Text>
                <Text style = {{fontSize: 20, color: '#fff'}} onPress = {this.props.saveEdit}>Done</Text>
            </View>
        )
    }
    
}

class HeaderOnNoEdit extends Component {
    render() {
        return (
            <View style = {styles.headers}>
                <TouchableOpacity onPress = {this.props.toggleMenu}>
                    <Image style = {{height: 35, width: 35}} source = {require('../../../img/icons/navigation/outline_clear_all_white_48dp.png')} />
                </TouchableOpacity>
                <Text style = {{fontSize: 20, color: '#fff'}} onPress = {this.props.edit}>Edit</Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
	container: {
		width: '100%',
		paddingTop: 0,
		padding: 10,
		flex: 1, 
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
	},
    headers: {
        width: '100%',
        height: 50,
        paddingTop: 20,
        padding: 10,
        marginBottom: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    input: {
        height: '100%',
        fontSize: 22,
        paddingLeft: 3,
        borderColor: 'transparent',
        borderBottomWidth: 1,
        borderColor: '#fff',
        color: '#fff',
    },
    input2: {
        height: 50,
        backgroundColor: '#fff',
        marginHorizontal: 10,
        marginVertical: 5,
        paddingVertical: 5,
        // paddingHorizontal: 15,
        width: window.width - 30,
    }
})