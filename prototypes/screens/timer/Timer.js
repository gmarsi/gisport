import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, TouchableOpacity, ImageBackground, StatusBar, Image } from 'react-native';
import { Stopwatch, Timer } from 'react-native-stopwatch-timer';
import SideMenu from 'react-native-side-menu';
import Menu from '../../components/menu/Menu';


 
export default class TestApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stopwatchStart: false,
      totalDuration: 90000,
      stopwatchReset: false,
      isOpen: false,

    };
    this.toggle = this.toggle.bind(this);
    this.toggleStopwatch = this.toggleStopwatch.bind(this);
    this.resetStopwatch = this.resetStopwatch.bind(this);
  }

 
  toggleStopwatch() {
    this.setState({stopwatchStart: !this.state.stopwatchStart, stopwatchReset: false});
  }
 
  resetStopwatch() {
    this.setState({stopwatchStart: false, stopwatchReset: true});
  }
  
  getFormattedTime(time) {
      this.currentTime = time;
  }


    toggle() {
        this.setState({
          isOpen: !this.state.isOpen,
        });
    }

    updateMenuState(isOpen) {
        this.setState({ isOpen });
    }

    onMenuItemSelected(item) {
        this.setState({
          isOpen: false,
        })
         
        const { navigate } = this.props.navigation;
        switch (item)
        {
        case "Profile":
            navigate('Profile')
            break;
        case 'Health':
            alert('Health')
            break;
        case 'Training':
            alert('Training')
            break;
        case 'Routines':
            navigate('MyRoutines')
            break;
        case 'Timer':
            navigate('Timer')
            break;
        case 'Settings':
            alert('Settings')
            break;
        case 'About':
            navigate('About')
            break;
        case 'Exercices':
            navigate('Exercices')
            break;
        case 'addRoutine':
            navigate('NewRoutine')
            break;
        default:
            navigate('Home')
        }

    }
 
  render() {
    const menu = <Menu onItemSelected={(item) => this.onMenuItemSelected(item)}  />;

    return (

      <SideMenu
        menu={menu}
        isOpen={this.state.isOpen}
        onChange={isOpen => this.updateMenuState(isOpen)} 
        disableGestures = {true}>
        <ImageBackground source = {require('../../../img/prozis.png')} style= {{width: '100%', height: '100%'}} >
        // <View style = {{backgroundColor: 'rgba(0,0,0,0.9)'}}>
        <StatusBar barStyle = 'light-content' />
          <View style = {styles.header}>
            <TouchableOpacity onPress = {() => this.toggle()}>
                <Image style = {{height: 35, width: 35}} source = {require('../../../img/icons/navigation/outline_clear_all_white_48dp.png')} />
            </TouchableOpacity>
          </View>
          <View style= {{padding: 0, justifyContent: 'space-between', height: '80%', width: '100%'}}>
            <View style = {{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View style = {{width: '50%', height: 200, paddingLeft: 10}}>
                <Text style = {{fontSize: 72, color: 'white'}}>min</Text>
                <Text style = {{fontSize: 72, color: 'white'}}>sec</Text>
                <Text style = {{fontSize: 72, color: 'white'}}>ms</Text>


              </View>
              <View style = {{width: '50%'}}>
                  <Stopwatch laps msecs start={this.state.stopwatchStart}
                    reset={this.state.stopwatchReset}
                    options={options}
                    getTime={this.getFormattedTime} />
              </View>
            </View>
            <View style = {styles.buttonsContainer}>

            {
              this.state.stopwatchStart
              ?
              <TouchableOpacity activeOpacity = {0.8} style = {[styles.buttons, {backgroundColor: 'rgba(255,0,0,0.8)'}]} onPress={this.resetStopwatch}>
                <Text style={{fontSize: 30, textAlign: 'center', color: '#fff'}}>Reset</Text>
              </TouchableOpacity>
              :
              <TouchableOpacity activeOpacity = {0.8} style = {[styles.buttons, {backgroundColor: '#27A745'}]} onPress={this.toggleStopwatch}>
                <Text style={{fontSize: 30, textAlign: 'center', color: '#fff'}}>{!this.state.stopwatchStart ? "Start" : "Stop"}</Text>
              </TouchableOpacity>
              
              
            }

            </View>
          </View>
        // </View>
        </ImageBackground>
      </SideMenu>
    );
  }
}
 
const options = {
  container: {
    padding: 5,
    borderRadius: 5,
    width: '100%',
    marginTop: 0, 
  },
  text: {
    fontSize: 72,
    color: '#FFF',
    textAlign: 'center'
  }
};

const styles = StyleSheet.create({
    buttonsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    buttons: {
        width: '100%',
        height: 50,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#fff',
        justifyContent: 'center',
        marginBottom: 50,

    },
    header: {
        width: '100%',
        height: '20%',
        paddingTop: 0,
        paddingHorizontal: 10, 
        marginBottom: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
})

      //   <ImageBackground source = {require('../../../img/prozis.png')} style= {{width: '100%', height: '100%'}} >

      // </ImageBackground>
