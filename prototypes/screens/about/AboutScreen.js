import React, { Component } from 'react'
import { View, Text, TouchableOpacity, ScrollView, StyleSheet, Image,  StatusBar, ImageBackground, Linking } from 'react-native'
import SideMenu from 'react-native-side-menu'
import Menu from '../../components/menu/Menu'

import { withNavigation } from 'react-navigation'




class AboutScreen extends Component {

    constructor(props) {
      super(props);
      this.toggle = this.toggle.bind(this);
      this.state = {
          isOpen: false,
      };
    }


    toggle() {
        this.setState({
          isOpen: !this.state.isOpen,
        });
    }

    updateMenuState(isOpen) {
        this.setState({ isOpen });
    }

    onMenuItemSelected(item) {
        this.setState({
          isOpen: false,
        })
         
        const { navigate } = this.props.navigation;
        
        switch (item)
        {
        case "Profile":
            navigate('Profile')
            break;
        case 'Health':
            alert('Health')
            break;
        case 'Training':
            alert('Training')
            break;
        case 'Routines':
            navigate('MyRoutines')
            break;
        case 'Timer':
            navigate('Timer')
            break;
        case 'Settings':
            alert('Settings')
            break;
        case 'About':
            navigate('About')
            break;
        case 'addRoutine':
            navigate('NewRoutine')
            break;
        case 'Exercices':
            navigate('Exercices')
            break;
        case 'Clear':
            this.clearAll()
            break;
        default:
            navigate('Home')
        }

    }

    watchThePump() {
        const url = 'https://www.youtube.com/watch?v=84cVizR6sPQ'
        Linking.canOpenURL(url).then(supported => {
          if (!supported) {
            console.log('Can\'t handle url: ' + url);
          } else {
            return Linking.openURL(url);
          }
        }).catch(err => console.error('An error occurred', err));
    }

    openLink(url) {
        Linking.canOpenURL(url).then(supported => {
          if (!supported) {
            console.log('Can\'t handle url: ' + url);
          } else {
            return Linking.openURL(url);
          }
        }).catch(err => console.error('An error occurred', err));
    }

  render() {
    const menu = <Menu onItemSelected={(item) => this.onMenuItemSelected(item)}  />;
    return (
      <SideMenu
        menu={menu}
        isOpen={this.state.isOpen}
        onChange={isOpen => this.updateMenuState(isOpen)} 
        disableGestures = {true}
      >
        <View style={styles.containerMenu}>
            <StatusBar barStyle = 'light-content' />
            <ImageBackground source = {require('../../../img/prozis.png')} style={{width: '100%', height: '100%'}}>
                <View style = {styles.container}>
                    <View style = {styles.header}>
                        <TouchableOpacity onPress = {() => this.toggle()}>
                            <Image style = {{height: 35, width: 35}} source = {require('../../../img/icons/navigation/outline_clear_all_white_48dp.png')} />
                        </TouchableOpacity>

                    </View>
                    <View style = {styles.content}>
                            <ScrollView>
                            <Text style = {{color: '#fff', fontSize: 32, textAlign: 'center'}}>About PUMP</Text>  
                            <View style = {styles.description}>
                                <Text style = {{textAlign: 'justify', color: '#fff', fontSize: 20, marginBottom: 30}}>Pump Routines v 1.1.1</Text>
                                <Text style = {{textAlign: 'justify', color: '#fff', fontSize: 20, marginBottom: 30}}>Contact: gmarsi.code@gmail.com</Text>
                                <Text style = {{textAlign: 'justify', color: '#fff', fontSize: 20, marginBottom: 30}}>
                                    All the images are under Creative Commons Licence <Text style = {{color: 'rgba(255,70,70,1)'}} onPress = {() => this.openLink('https://creativecommons.org/licenses/by-sa/3.0/deed.en')}>Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) </Text>. Credits to 
                                    <Text style = {{color: 'rgba(255,70,70,1)'}} onPress = {() => this.openLink('https://www.wger.de')}> www.wger.de</Text>
                                </Text>
                          </View>  
                        </ScrollView>
                    </View>

                </View>
            </ImageBackground>
        </View>        
      </SideMenu>
    );
  }
}

export default withNavigation(AboutScreen)



const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'rgba(0,0,0,0.8)',

    },
    header: {
        width: '100%',
        height: 50,
        paddingTop: 50,
        padding: 10, 
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    content: {
        paddingHorizontal: 10,
        width: '100%'
    },
    description: {
        // backgroundColor: 'rgba(0,0,0,0.8)',
        width: '100%',
        height: '100%',
        padding: 5,
        borderRadius: 10,
        marginTop: 20,
        paddingBottom: 100
    }
})

