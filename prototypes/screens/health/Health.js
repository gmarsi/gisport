import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native'
import { ImagePicker } from 'expo';
import SideMenu from 'react-native-side-menu';
import Menu from '../../components/menu/Menu'


class ProfileRow extends Component {
    render() {
        return(
            <View style = {{alignItems: 'flex-start', width: '100%', marginBottom: 15,}}>
                <Text style = {{fontSize: 16}}>{this.props.title}</Text>
                <View style = {{width: '100%', height: 50,   marginTop: 5}}>
                    <TextInput
                        style={{height: 40, fontSize: 22, borderColor: 'gray', borderBottomWidth: 1, borderColor: 'red'}}
                        onChangeText={(text) => this.setState({text})}
                        value={this.props.value}
                        editable = {this.props.editable}
                      />
                </View>
            </View>
        )
    }
}


export default class HealthScreen extends Component {

    constructor(props) {
        super(props)
        this.state = { 
            name: 'Useless placeholder',
            lastname: '',
            username :'',
            email: '',
            phone: '',
            gender :'',
            edit: false,
            image: null
        }
    }

    editMode() {
        this.setState({edit: !this.state.edit})
        this.editDisabled()
        this.editEnabled()
    }

    editEnabled() {
        if(!this.state.edit) {
            return ({display: 'none'})
        }else {
            return ({})
        }
    }
    editDisabled() {
        if(this.state.edit) {
            return ({display: 'none'})
        }else {
            return ({})
        }
    }

    ImagePicker() {
        alert('Image picker')
    }



    render() {
        console.log(this.state.edit)
        return (
            
            <ScrollView>
                <View style = {styles.container}>
                    {/* Displayed when state.edit == true */}
                    <View style = {[{width: '100%', height: 50, marginBottom: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}, this.editDisabled()]}>
                        <Image style = {{height: 35, width: 35}} source = {require('../../../img/icons/navigation/outline_clear_all_black_48dp.png')} />
                        <Text style = {{fontSize: 20}} onPress = {() => this.editMode()}>Edit</Text>
                    </View>     
                    {/* Displayed when state.edit == false */}
                    <View style = {[{width: '100%', height: 50, marginBottom: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}, this.editEnabled()]}>
                        <Text style = {{fontSize: 20}}>Cancel</Text>
                        <Text style = {{fontSize: 20}} onPress = {() => this.editMode()}>Done</Text>
                    </View>
                    <View style = {{width: '100%', height: 150, marginBottom: 10,  alignItems: 'center', justifyContent: 'center'}}>
                        <View style = {{height: 100, width: 100, borderRadius: 0, borderWidth: 1}}></View>
                        <Text onPress = {() => this.ImagePicker()} style = {{fontSize: 16, fontWeight: 'bold', color: 'red', width: '100%', textAlign: 'center', marginTop: 15}}>Change Profile Photo</Text>
                    </View>
                    <View style = {{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                        <ProfileRow title = {'Name'} value = 'Marcos' editable = {false} />
                        <ProfileRow title = {'Lastname'} value = 'Gimenez Lopez' />
                        <ProfileRow title = {'Username'} value = 'gmarsi' />    
                    </View>
                    <View style = {{marginTop: 50, marginBottom: 30}}>
                        <Text style = {{fontSize: 20, fontWeight: 'bold'}}> 
                            Private Information
                        </Text>
                    </View>
                    <View style = {{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                        <ProfileRow title = {'Email address'} value = 'gmarsi.code@gmail.com' />
                        <ProfileRow title = {'Phone'} value = '+34 671 91 55 04' />
                        <ProfileRow title = {'Gender'} value = 'Male' />    
                    </View>
                </View>
            </ScrollView>
            
        )
    }
}


const styles = StyleSheet.create({
    container: {
        width: '100%',
        paddingTop: 20,
        padding: 10,
        flex: 1, 
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    }
})