import React, { Component } from 'react'
import { View, Text, TouchableOpacity, FlatList, ScrollView, TextInput, StyleSheet, Image, ImageBackground, AsyncStorage } from 'react-native'
import API from '../../API'
import { createStackNavigator } from 'react-navigation'
import SideMenu from 'react-native-side-menu'
import Menu from '../../components/menu/Menu'
import LANG from '../../global'

class Exercice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            exerciceName: this.props.exerciceName,
            id: this.props.id,
            image: this.props.image,
            images: this.props.images,
            muscle: this.props.muscle,
            primary: this.props.primary,
            secondary: this.props.secondary,
            level: this.props.level,
            color: {borderColor: 'green'},
            textDescription: this.props.description,
            description: false
        }
    }

        selected() {
        if (this.state.description) {
            return { transform: [{ rotate: '180deg'}] }
        }else {
            return { transform: [{ rotate: '0deg'}] }
        }
    }


    render() {
        return (
            <View style = {styles.exercice}>
                <View style= {{width: '100%'}}>
                    <Text style = {{fontWeight: 'bold', fontSize: 18, width: '100%', height: 20, }} numberOfLines = {1}>{this.state.exerciceName}</Text>
                </View>
                <View style = {{flexDirection: 'row'}}>
                    <View style = {styles.imageContainer}>
                        <Image source = {this.state.image} style = {styles.exerciceImage}/>
                    </View>
                    <View style = {styles.exerciceDetails}>                        
                        <Text style = {{fontWeight: 'bold', fontSize: 16, width: '100%', height: 20, }}>Muscle group: 
                            <Text style = {{fontWeight: 'normal', fontSize: 16, width: '100%', height: 20, }}> {this.state.muscle}</Text>
                        </Text>
                        {/*<Text style = {{fontWeight: 'bold', fontSize: 16, width: '100%', height: 20, }}>Level: 
                            <Text style = {{fontWeight: 'normal', fontSize: 16, width: '100%', height: 20, }}> {this.state.level}/5</Text>
                        </Text>*/}
                    </View>
                </View>
                <TouchableOpacity onPress = {() => this.setState({description: !this.state.description})} activeOpacity = {0.8} style = {{width: '100%', height: 30, borderRadius: 10, alignItems: 'center', justifyContent: 'center', marginBottom: 0, backgroundColor: 'rgb(220,220,220)'}}>
                    <Image source = {require('../../../img/icons/navigation/baseline_expand_more_black_48dp.png')} style = {[{width: 20, height: 20}, this.selected()]} />
                </TouchableOpacity>
                {
                    this.state.description ?

                    <View style = {{width: '100%', marginTop: 10}}>

                        <Text style = {{fontSize: 18, fontWeight: 'bold'}}>{this.state.exerciceName}</Text>

                        {/*<Text style = {{fontWeight: 'bold', marginTop: 10}}>Diff level: <Text style = {{fontWeight: 'normal'}}>{this.state.level}/5</Text></Text> */}
                        <Text style = {{fontWeight: 'bold'}}>Primary muscle: <Text style = {{fontWeight: 'normal'}}>{this.state.primary}</Text></Text>
                        <Text style = {{fontWeight: 'bold'}}>Secondary muscle: <Text style = {{fontWeight: 'normal'}}>{this.state.secondary}</Text></Text>

                        <View style = {{flexDirection: 'row', marginVertical: 10}}>
                            <ScrollView horizontal = {true} automaticallyAdjustContentInsets = {true} >
                                <Image source = {this.state.images[0]} style = {{width: 192, height: 192}} />
                                <Image source = {this.state.images[1]} style = {{width: 192, height: 192}} />
                            </ScrollView>
                        </View>
                        <Text style = {{fontWeight: 'bold'}}>
                            {/*Description:{this.state.description}*/}
                        </Text>
                         <Text style = {{textAlign: 'justify', marginTop: 5}}>
                            {/*this.state.textDescription */}
                        </Text>
                   



                    </View>

                    :

                    null
                }
            </View>
        )
    }
}

class RenderExercices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            EXERCICES: [],
            group: this.props.group,
            displayed: false
        }
    }    

    renderData(){ 
        return this.state.EXERCICES 
    }

    selected() {
        if (this.state.displayed) {
            return { transform: [{ rotate: '180deg'}] }
        }else {
            return { transform: [{ rotate: '0deg'}] }
        }
    }

    _renderExercices = (item) =>  {
        const object = item.item
        const name = object.name
        const mainImage = object.image[0]
        const images = object.image
        const muscle = object.group
        const level = object.level
        const primaryMuscle = object.primaryMuscle
        const secondaryMuscle = object.secondaryMuscle
        const description = object.description
        const id = object.id.toString()
        return (
            <Exercice {...item} exerciceName = {name} primary = {primaryMuscle} secondary = {secondaryMuscle} description = {description} image = {mainImage} images = {images} level = {level} muscle = {muscle} id = {id}/>
        )
    }

    toggleExercices() {
        if(this.state.displayed) {
            return { height: 'auto' }
        }else {
            return { height: 0}
        }
    }

    renderEmpty = () => <View style = {{marginTop: 50, alignItems: 'center', justifyContent: 'center'}}><Text style = {{color: '#fff'}}>You have no exercices yet</Text></View>
    
    itemSeparator = () => <View style = {{width: '100%', height: 20}}></View>

    render() {
        return (
            <View style = {{width: '100%', paddingHorizontal: 10, marginBottom: 0}}>
                <TouchableOpacity onPress = {() => this.setState({displayed: !this.state.displayed})} activeOpacity = {0.8}>
                    <View style = {{width: '100%', marginBottom: 10, marginTop: 10, backgroundColor: 'rgba(255,255,255,0.6)', borderRadius: 6, padding: 10,
                                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Text style= {{fontSize: 26, color: '#fff'}}>{this.state.group}</Text>
                        <Image source = {require('../../../img/icons/navigation/baseline_expand_more_white_48dp.png')} style = {[{width: 20, height: 20}, this.selected()]} />
                    </View>
                </TouchableOpacity>
                <View style = {this.toggleExercices()}>
                    <FlatList   keyExtractor = { item => item.id.toString() }
                                data = {this.props.list}
                                ListEmptyComponent = {this.renderEmpty}
                                ItemSeparatorComponent = {this.itemSeparator}
                                renderItem = {this._renderExercices} />
                </View>     
            </View>
        )
    }
}

export default class ExercicesScreen extends Component {

    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            routineName: '',
            chestExercices: '',
            backExercices: '',
            legsExercices: '', 
            armsExercices: '', 
            shouldersExercices: '', 
            absExercices: '',
            MY_ROUTINES: [],
            isOpen: false,
            lang: ''
           
        }
    }    

    componentWillMount() {
        this.setState({lang: LANG.getLang()})
        setTimeout(() => {
            console.log('exercices', this.state.lang)
        })
        const CHEST_EXERCICES = API.getChestExercices()
        const BACK_EXERCICES = API.getBackExercices()
        const LEGS_EXERCICES = API.getLegExercices()
        const ARMS_EXERCICES = API.getArmsExercices()
        const SHOULDERS_EXERCICES = API.getShouldersExercices()
        const ABS_EXERCICES = API.getAbsExercices()
        this.setState({chestExercices: CHEST_EXERCICES})
        this.setState({backExercices: BACK_EXERCICES})
        this.setState({legsExercices: LEGS_EXERCICES})
        this.setState({armsExercices: ARMS_EXERCICES})
        this.setState({absExercices: ABS_EXERCICES})
        this.setState({shouldersExercices: SHOULDERS_EXERCICES})
    }

    toggle() {
        console.log('open')
        this.setState({
          isOpen: !this.state.isOpen,
        });
    }

    updateMenuState(isOpen) {
        this.setState({ isOpen });
    }

    onMenuItemSelected(item) {
        this.setState({
          isOpen: false,
        })
         
        const { navigate } = this.props.navigation;
        const { push } = this.props.navigation;


        switch (item)
        {
        case "Profile":
            navigate('Profile')
            break;
        case 'Health':
            alert('Health')
            break;
        case 'Training':
            alert('Training')
            break;
        case 'Routines':
            navigate('MyRoutines')
            break;
        case 'Timer':
            navigate('Timer')
            break;
        case 'Settings':
            alert('Settings')
            break;
        case 'About':
            navigate('About')
            break;
        case 'addRoutine':
            navigate('NewRoutine')
            break;
        case 'Exercices':
            navigate('Exercices')
            break;
        default:
            navigate('Home')
        }

    }


    render() {
        const ImgMenu = require('../../../img/icons/navigation/outline_clear_all_white_48dp.png');
        const menu = <Menu onItemSelected={(item) => this.onMenuItemSelected(item)}  />;
    return (
          <SideMenu
            menu={menu}
            isOpen={this.state.isOpen}
            onChange={isOpen => this.updateMenuState(isOpen)} 
            disableGestures = {true} >
            <ImageBackground source = {require('../../../img/prozis.png')} style={{width: '100%', height: '100%'}} >
                        <View style = {{width: '100%', paddingHorizontal: 10, paddingTop: 40, marginBottom: 15, height: 50, flexDirection: 'row',  justifyContent: 'space-between', alignItems: 'center'}}>
                            <TouchableOpacity onPress = {() => this.toggle()} activeOpacity = {0.8}>
                                <Image style = {{width: 35, height: 35}} source = {ImgMenu} />
                            </TouchableOpacity>
                        </View>
                <ScrollView>
                       
                        {
                        this.state.lang['_55'] == 'ES'  ?
                        <View style = {styles.container}> 
                            <RenderExercices list = {this.state.absExercices} group = {'Abdomen'}/>
                            <RenderExercices list = {this.state.armsExercices} group = {'Brazos'}/>
                            <RenderExercices list = {this.state.backExercices} group = {'Espalda'}/>
                            <RenderExercices list = {this.state.shouldersExercices} group = {'Hombros'}/>
                            <RenderExercices list = {this.state.chestExercices} group = {'Pecho'}/>                        
                            <RenderExercices list = {this.state.legsExercices} group = {'Pierna y glúteo'}/>
                        </View> 
                        : 
                        <View style = {styles.container}> 
                            <RenderExercices list = {this.state.absExercices} group = {'Abs'}/>
                            <RenderExercices list = {this.state.armsExercices} group = {'Arms'}/>
                            <RenderExercices list = {this.state.backExercices} group = {'Back'}/>
                            <RenderExercices list = {this.state.chestExercices} group = {'Chest'}/>                        
                            <RenderExercices list = {this.state.legsExercices} group = {'Legs and glutes'}/>
                            <RenderExercices list = {this.state.shouldersExercices} group = {'Shoulders'}/>
                        </View>

                        }           

                                       
                </ScrollView>
            </ImageBackground>
             </SideMenu>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        paddingTop: 20,
        flex: 1,
        alignItems: 'center',
    },
    searchView: {
        width: '100%',
        height: 35,
        flexDirection: 'row',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#rgba(255,255,255,0.5)',
        borderColor: '#fff',
        marginBottom: 30
    },
    searchBar: {
        width: '85%',
        height: 35,
        backgroundColor: 'transparent',
        fontSize: 22,
        paddingLeft: 8,
        color: '#fff'
    },
    exercice: {
        width: '100%',
        // height: 180,
        padding: 10,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'flex-start',
        shadowColor: '#000000',
        shadowOpacity: 0.6,
        shadowRadius: 3,
        shadowOffset: {
            width: 2,
            height: 2
        },
        elevation: 3,
    },
    imageContainer: {
        flex: 1,
        width: '40%',
        height: 110,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },
    exerciceImage: {
        width: 110,
        height: 110
    },
    exerciceDetails: {
        paddingTop: 5,
        height: 110,
        width: '60%',
        overflow: 'hidden' 
    },
    exerciceButton: {
        width: '100%',
        height: 50,
        borderWidth: 1,
        borderRadius: 13,
        alignItems: 'center',
        justifyContent: 'center'
    }
})