import React, { Component } from 'react'
import { View, Text, TouchableOpacity, FlatList, ScrollView, TextInput, StyleSheet, Image, ImageBackground, AsyncStorage } from 'react-native'
import API from '../../API'
import { createStackNavigator } from 'react-navigation'

var array = []
var fullRoutine = {}

class Exercice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            exerciceName: this.props.exerciceName,
            id: this.props.id,
            image: this.props.image,
            muscle: this.props.muscle,
            level: this.props.level,
            color: {borderColor: 'green'}
        }
    }

    componentWillMount() {
        this.setState({selected: false})
    }    
    save(item) {
        let counter = 0
        this.setState({selected: !this.state.selected})
        // // para saber si el elemento esta en el array
        for(let i = 0; i < array.length; i++) {
            if (array[i].id == item) {
                counter++
            }
        }
        // si no esta en el array lo metemos
        if(counter == 0) {
            const newExercice = {
                'id' : item
            }
            array.push(newExercice)
            this.setState({color: { borderColor: 'red'} }) 
        }
        // si esta en el array lo sacamos
        else {  
            for(let i = 0; i < array.length; i++) {
                if ( array[i].id == item) {
                    array.splice(i, 1)
                }
            }
            this.setState({color: { borderColor: 'green'} }) 
        }
    }

    render() {
        return (
            <View style = {styles.exercice}>
                <View style = {styles.imageContainer}>
                    <Image source = {this.state.image} style = {styles.exerciceImage}/>
                </View>
                <View style = {styles.exerciceDetails}>
                    <Text style = {{fontWeight: 'bold', fontSize: 18, width: '100%', height: 40, }} numberOfLines = {2}>{this.state.exerciceName}</Text>
                    <Text style = {{fontWeight: 'bold', fontSize: 16, width: '100%', height: 20, }}>Muscle: 
                        <Text style = {{fontWeight: 'normal', fontSize: 16, width: '100%', height: 20, }}> {this.state.muscle}</Text>
                    </Text>
                    <Text style = {{fontWeight: 'bold', fontSize: 16, width: '100%', height: 20, }}>Level: 
                        <Text style = {{fontWeight: 'normal', fontSize: 16, width: '100%', height: 20, }}> {this.state.level}/5</Text>
                    </Text>
                    <View style = {{width: '100%', marginTop: 5, }}>
                        <TouchableOpacity style = {{width: '100%'}} onPress = {() => this.save(this.state.id)}>                         
                            <View style = {[styles.exerciceButton, this.state.color]}>
                                <Text style = {{fontSize: 32, textAlign: 'center' }}>
                                    {
                                        this.state.selected 
                                        ? 
                                        <Text style = {{color: 'red'}}>REMOVE</Text>
                                        :
                                        <Text style = {{color: 'green'}}>ADD</Text>
                                    }
                                </Text>
                            </View>                           
                        </TouchableOpacity>
                    </View>                    
                </View>
            </View>
        )
    }
}

class RenderExercices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            EXERCICES: [],
            group: this.props.group,
            displayed: false
        }
    }    

    renderData(){ 
        return this.state.EXERCICES 
    }

    selected() {
        if (this.state.displayed) {
            return { transform: [{ rotate: '180deg'}] }
        }else {
            return { transform: [{ rotate: '0deg'}] }
        }
    }

    _renderExercices = (item) =>  {
        const object = item.item
        const name = object.name
        const image = object.image[0]
        const muscle = object.group
        const level = object.level
        const id = object.id.toString()
        return (
            <Exercice {...item} exerciceName = {name} image = {image} level = {level} muscle = {muscle} id = {id}/>
        )
    }

    toggleExercices() {
        if(this.state.displayed) {
            return { height: 'auto' }
        }else {
            return { height: 0}
        }
    }

    renderEmpty = () => <View style = {{marginTop: 50, alignItems: 'center', justifyContent: 'center'}}><Text style = {{color: '#fff'}}>You have no exercices yet</Text></View>
    
    itemSeparator = () => <View style = {{width: '100%', height: 20}}></View>

    render() {
        return (
            <View style = {{width: '100%', paddingHorizontal: 10, marginBottom: 0}}>
                <TouchableOpacity onPress = {() => this.setState({displayed: !this.state.displayed})} activeOpacity = {0.8}>
                    <View style = {{width: '100%', marginBottom: 10, marginTop: 10, backgroundColor: 'rgba(255,255,255,0.6)', borderRadius: 6, padding: 10,
                                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Text style= {{fontSize: 26, color: '#fff'}}>{this.state.group}</Text>
                        <Image source = {require('../../../img/icons/navigation/baseline_expand_more_white_48dp.png')} style = {[{width: 20, height: 20}, this.selected()]} />
                    </View>
                </TouchableOpacity>
                <View style = {this.toggleExercices()}>
                    <FlatList   keyExtractor = { item => item.id.toString() }
                                data = {this.props.list}
                                ListEmptyComponent = {this.renderEmpty}
                                ItemSeparatorComponent = {this.itemSeparator}
                                renderItem = {this._renderExercices} />
                </View>     
            </View>
        )
    }
}

export default class CreateRoutineScreen extends Component {

    state = {
        routineName: '',
        chestExercices: '',
        backExercices: '',
        legsExercices: '', 
        armsExercices: '', 
        shouldersExercices: '', 
        absExercices: '',
        MY_ROUTINES: [],
       
    }

    getDate() {
        const DATE = new Date()
        const DAY = DATE.getDay() + 1
        const MONTH = DATE.getMonth() + 1
        const YEAR = DATE.getFullYear()
        const TODAY = `${DAY}-${MONTH}-${YEAR}`
        return TODAY
    }

    componentWillMount() {
        const CHEST_EXERCICES = API.getChestExercices()
        const BACK_EXERCICES = API.getBackExercices()
        const LEGS_EXERCICES = API.getLegExercices()
        const ARMS_EXERCICES = API.getArmsExercices()
        const SHOULDERS_EXERCICES = API.getShouldersExercices()
        const ABS_EXERCICES = API.getAbsExercices()
        this.getDate()
        this.setState({chestExercices: CHEST_EXERCICES})
        this.setState({backExercices: BACK_EXERCICES})
        this.setState({legsExercices: LEGS_EXERCICES})
        this.setState({armsExercices: ARMS_EXERCICES})
        this.setState({shouldersExercices: SHOULDERS_EXERCICES})
        this.setState({absExercices: ABS_EXERCICES})
        this.initializeEmptyRoutineArray()
    }

    finish() {
        this.clearAll()
        this.props.navigation.navigate('Home', {refreshData: true})
    }

    initializeEmptyRoutineArray = async () => {
        try {
            const MY_STORED_ROUTINES = await AsyncStorage.getItem('@Pump:MY_ROUTINES')
            const ITEM = JSON.parse(MY_STORED_ROUTINES)
            if(MY_STORED_ROUTINES) {
                this.setState({MY_ROUTINES: ITEM})
            }
        }catch(error) {
            console.log('ERROR SETTING ITEM: ', error)
        }
    }

    _storeData = async () => {        
        try {
            await AsyncStorage.setItem('@Pump:MY_ROUTINES', this.state.MY_ROUTINES);
            this.finish()
        } catch (error) {
            console.log('ERROR SETTING ITEM: ', error)
        }
    }

    _storeArrayData() {
        let arrayLenght = routineName = 0
        if(array.length == 0) {
            alert('You must select at least 1 exercices')
        }else {
            arrayLenght = 1
        }
        const name = this.state.routineName
        if(name == '') {
            alert('You must set a name for your routine')
        }else {
            routineName = 1
        }
        if(routineName == 1 && arrayLenght == 1) {

            if(!this.state.MY_ROUTINES){
                fullRoutine.exercices = array
                fullRoutine.id = '1'
                fullRoutine.name = this.state.routineName
                fullRoutine.date = this.getDate()
                console.log('ROUTINES', fullRoutine)
                
                this.setState({MY_ROUTINES: JSON.stringify(fullRoutine)})
                setTimeout(() => {
                  this._storeData()
                }, 100);
            }else {
                const EXISTING_ROUTINES = this.state.MY_ROUTINES
                fullRoutine.exercices = array
                fullRoutine.id = this.createNewId(EXISTING_ROUTINES)
                fullRoutine.name = this.state.routineName
                fullRoutine.date = this.getDate()
                EXISTING_ROUTINES.push(fullRoutine)
                console.log('ROUTINES', EXISTING_ROUTINES)
                this.setState({MY_ROUTINES: JSON.stringify(EXISTING_ROUTINES)})
                setTimeout(() => {
                  this._storeData()
                }, 100);
            }
        }
    }

    createNewId(routines) {
        let ID = Math.random() * 1000

        for(let i = 0; i < routines.length; i++) {
            if(routines[i].id == ID) {
                ID = Math.random() * 1000
                i = 0
            }
        }

        const returnValue = 'R_' + (ID.toFixed(0)).toString()
        return returnValue
    }

    setRoutineName(name) {
        this.setState({routineName: name})
    }

    clearAll = async () => {
        array = []
        fullRoutine = {}
        this.setState({routineName: ''})
        this.setState({exercice: ''})
        this.setState({MY_ROUTINES: []})
    }

    render() {
        const searchImage = require('../../../img/icons/navigation/outline_search_white_48dp.png')
        return (
            <ImageBackground source = {require('../../../img/prozis.png')} style={{width: '100%', height: '100%'}} >
                <ScrollView>
                    <View style = {styles.container}>
                        <View style = {{width: '100%', paddingHorizontal: 10,marginBottom: 15, height: 50, flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
                            <Text style = {{fontSize: 24, color: '#fff', fontWeight: 'normal'}} onPress = {() => this.props.navigation.navigate('Home')}>Cancel</Text>
                            <Text style = {{fontSize: 24, color: 'yellow', fontWeight: 'normal'}} onPress = {() => this.clearAll()}>Clear</Text>
                            <Text style = {{fontSize: 24, color: '#fff', fontWeight: 'normal'}} onPress = {() => this._storeArrayData()}>Finish</Text>

                        </View>
                        <View style = {{width: '100%', paddingHorizontal: 10,marginTop: 10, marginBottom: 10}}>
                            <Text style = {{color: '#fff', fontSize: 22, marginBottom: 5}}>Routine name</Text>
                            <TextInput  onChangeText = {(name) => this.setState({routineName: name})} 
                                        underlineColorAndroid='transparent' 
                                        clearButtonMode = 'always'
                                        returnKeyType = 'done'
                                        value = {this.state.routineName}
                                        style = {{fontSize: 22, paddingHorizontal: 5, color: '#fff', width: '100%', height: 50, borderRadius: 5, borderWidth: 1, borderColor: '#fff'}}/>
                        </View>
                        <RenderExercices list = {this.state.absExercices} group = {'Abs'}/>
                        <RenderExercices list = {this.state.armsExercices} group = {'Arms'}/>
                        <RenderExercices list = {this.state.backExercices} group = {'Back'}/>
                        <RenderExercices list = {this.state.chestExercices} group = {'Chest'}/>                        
                        <RenderExercices list = {this.state.legsExercices} group = {'Legs and glutes'}/>
                        <RenderExercices list = {this.state.shouldersExercices} group = {'Shoulders'}/>

                    </View>                    
                </ScrollView>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        paddingTop: 20,
        flex: 1,
        alignItems: 'center',
    },
    searchView: {
        width: '100%',
        height: 35,
        flexDirection: 'row',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#rgba(255,255,255,0.5)',
        borderColor: '#fff',
        marginBottom: 30
    },
    searchBar: {
        width: '85%',
        height: 35,
        backgroundColor: 'transparent',
        fontSize: 22,
        paddingLeft: 8,
        color: '#fff'
    },
    exercice: {
        width: '100%',
        height: 150,
        padding: 10,
        backgroundColor: '#fff',
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: '#000000',
        shadowOpacity: 0.6,
        shadowRadius: 3,
        shadowOffset: {
            width: 2,
            height: 2
        },
        elevation: 3,
    },
    imageContainer: {
        flex: 1,
        width: '50%',
        height: 150,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },
    exerciceImage: {
        width: 140,
        height: 140
    },
    exerciceDetails: {
        height: 140,
        width: '50%',
        overflow: 'hidden' 
    },
    exerciceButton: {
        width: '100%',
        height: 50,
        borderWidth: 1,
        borderRadius: 13,
        alignItems: 'center',
        justifyContent: 'center'
    }
})