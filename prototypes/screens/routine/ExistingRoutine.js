import React, { Component } from 'react'
import {Alert, View, Text, TouchableOpacity, ScrollView, TextInput, StyleSheet, Image, ImageBackground, AsyncStorage, FlatList, Dimensions, Platform} from 'react-native'
import { createStackNavigator } from 'react-navigation'
import API from '../../API'

import { Stopwatch } from 'react-native-stopwatch-timer';


var array = []
var removeSet = false
const window = Dimensions.get('window')
const keyboardSpace = window.height / 2
var reset = false

class ExerciceButtons extends Component {

    constructor(props) {
        super(props);    
        this.state = {
            colorRight: this.props.colorRight || '',
            colorLeft: this.props.colorLeft || '',
            removeSet: this.props.removeSet
        };
    }

    render() {
        return (
                <View style = {{width: '100%', height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 10, paddingHorizontal: 10}}>
                    <TouchableOpacity style = {[{borderRadius: 3, width: '50%', height: 50, }, this.state.colorLeft]} onPress = {this.props.actionLeft}>
                        <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%'}}>
                            <Image source = {this.props.imageLeft} style = {{width: 30, height: 30, marginRight: 5}} />
                            <Text style = {{fontSize: 16, color: '#fff'}}>{this.props.textLeft}</Text>
                        </View>
                    </TouchableOpacity>

                    { !removeSet ? 

                        <TouchableOpacity style = {[{borderRadius: 3, width: '45%', height: 50}, this.state.colorRight]} onPress = {this.props.actionRight}>
                            <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%'}}>
                                <Image source = {this.props.imageRight} style = {{width: 30, height: 30, marginRight: 5}} />
                                <Text style = {{fontSize: 16, color: '#fff'}}>{this.props.textRight}</Text>
                            </View>
                        </TouchableOpacity>


                        :

                        <TouchableOpacity style = {[{borderRadius: 3, width: '45%', height: 50}, this.state.colorRight]} onPress = {this.props.neutralAction}>
                            <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%'}}>
                                <Image source = {require('../../../img/icons/navigation/baseline_done_white_48dp.png')} style = {{width: 30, height: 30, marginRight: 5}} />
                                <Text style = {{fontSize: 16, color: '#fff'}}>Save</Text>
                            </View>
                        </TouchableOpacity>             

                    }

                </View>
        )
    }
}

class ExerciceOptions extends Component {

    constructor(props) {
        super(props);
        this.state = {
            images: this.props.images
        }
    }

    renderImages() {
        const images = this.state.images
        let imagesComponent = []
        for(let i = 0; i < images.length; i++) {
            const img = <View key = { (Math.random() * 100 ).toString()} style = {styles.image}>
                            <Image source = {images[i]} style = {{width: 200, height: 200, borderRadius: 10,}} />
                        </View>
            imagesComponent.push(img)
        }
        return imagesComponent
    }


    render() {
        return (
            <View style = {{width: '100%', height: 300,backgroundColor: '#fff'}}>
                <ScrollView horizontal = {true} style = {{width: '100%', height: '100%', padding: 10}}>
                    <View style = {{width: '100%', height: '100%',flexDirection: 'row', alignItems: 'flex-start', paddingVertical: 10,}}>
                        { this.renderImages() }
                    </View>                    
                </ScrollView>

            {/*                                NO ELIMINAR

                <ExerciceButtons textLeft= 'Remove Exercice' textRight = 'Details' 
                        imageLeft = {require('../../../img/icons/navigation/baseline_remove_white_48dp.png')}
                        imageRight = {require('../../../img/icons/navigation/outline_description_white_48dp.png')}
                        colorLeft = {styles.red}
                        colorRight =  {styles.gray}/>*/}                
            </View>
        )
    }
}

class Set extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.id,
            position: this.props.position,
            kg: this.props.kg || '',
            reps: this.props.reps || '',
            finished: false,
            setAsFinished: {backgroundColor: '#fff'},
            index: this.props.index,
            removed: false,
        }
    }

    componentWillMount() {
    }

    changeKg(kg) {
        this.setState({kg: kg})
        this.finishSet()
    }

    changeReps(reps) {
        this.setState({reps: reps})
        this.finishSet()

    }

    finishSet = async () => {
        const exerciceSets = await AsyncStorage.getItem(this.state.id)

        if(!exerciceSets) {
        }else {
            const parse = JSON.parse(exerciceSets)
            const newSet = {
                'kg': this.state.kg,
                'reps': this.state.reps
            }
            const position = this.state.position - 1
            parse[position] = newSet
            await AsyncStorage.setItem(this.state.id, JSON.stringify(parse))
        }
        
    }

    removeSet = async () => {
        try {
            const exerciceSets = await AsyncStorage.getItem(this.state.id)
            const parse = JSON.parse(exerciceSets)
            parse.splice(this.state.index, 1)
            await AsyncStorage.setItem(this.state.id, JSON.stringify(parse))
            this.setState({removed: true})
          
        }catch(error) {
            // console.log('ERROR REMOVING SET', error.message)
        }
        
    }



    setAsFinished() {
        this.setState({finished: !this.state.finished})
        setTimeout(() => {
            if(this.state.finished) {
                this.setState({setAsFinished: {backgroundColor: 'rgba(15,133,2,0.65)'}})
                reset = !reset
                console.log('reset', reset)
            }else {
                this.setState({setAsFinished: {backgroundColor: '#fff'} }) 
            }
        })

    }

    render() {
        return (
            <View>
            {
                !this.state.removed ?

                <View style = {{width: '100%', height: 50, flexDirection: 'row', alignItems: 'center', marginBottom: 10}}>
                    <View style = {{width: '15%', height: 50, justifyContent: 'center'}}>
                        <Text style = {{fontSize: 24, textAlign: 'center'}}>#{this.state.position}</Text>
                    </View>
                    <View style = {{width: '35%', height: 50, justifyContent: 'center'}}>
                        <TextInput placeholder = '0' placeholderTextColor = 'rgb(100,100,100)' returnKeyType = 'done' underlineColorAndroid='transparent' onChangeText = {kg => this.changeKg(kg) } keyboardType = {'numeric'} style = {{textAlign: 'center', height: 40, width: '80%', paddingLeft: 3, fontSize: 22, borderWidth: 1, borderRadius: 5}} value = {this.state.kg}/>
                    </View>
                    <View style = {{width: '35%', height: 50, justifyContent: 'center'}}>
                        <TextInput placeholder = '0' placeholderTextColor = 'rgb(100,100,100)' returnKeyType = 'done' underlineColorAndroid='transparent' onChangeText = {reps => this.changeReps(reps) } keyboardType = {'numeric'} style = {{textAlign: 'center', height: 40, width: '80%', paddingLeft: 3, fontSize: 22, borderWidth: 1, borderRadius: 5}} value = {this.state.reps}/>
                    </View>
                    { removeSet ?

                        <TouchableOpacity onPress = {() => this.removeSet()} style = {{width: '15%', height: 50, justifyContent: 'center',}}>
                            {/*<View  style = {[{width: 30, height: 30, borderRadius: 15, borderWidth: 1, backgroundColor: 'rgba(255,0,0,0.65)'},]}></View> */}
                            <Image source = {require('../../../img/icons/navigation/outline_delete_red_48dp.png')} style = {{width: 30, height: 30}} />
                        </TouchableOpacity>

                        :

                        <TouchableOpacity onPress = {() => this.setAsFinished()} style = {{width: '15%', height: 50, justifyContent: 'center',}}>
                            <View  style = {[{width: 30, height: 30, borderRadius: 15, borderWidth: 1}, this.state.setAsFinished]}></View>
                        </TouchableOpacity>
                    }
                </View>

                :

                null
            }
            </View>
                
        )
    }
}

class RenderSets extends Component {

    constructor(props) {
        super(props);
        this.state = {
            routineId: this.props.routineId,
            list: this.props.list,
            position: this.props.position,
        }
    }

    _renderExercices = (item) =>  {
        const position = (item.index + 1).toString()
        return (
            <Set {...item}  key = {item.index} 
                            index = {item.index} 
                            position = {position} 
                            id = {this.state.routineId} 
                            kg = {item.item.kg} 
                            reps = {item.item.reps}/>
        )
    }
    
    itemSeparator = () => <View style = {{width: '100%', height: 10}}></View>

    keyExtractor() {
       return (Math.random() * 100).toString()
    }

    render() {
        return (
            <View style = {{width: '100%', paddingHorizontal: 0, }}>
                <FlatList   
                    keyExtractor = {this.keyExtractor}
                    data = {this.props.list}
                    ItemSeparatorComponent = {this.itemSeparator}
                    renderItem = {this._renderExercices}/>
            </View>
        )
    }
}

class ExerciceDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sets: this.props.sets,
            id: this.props.id,
            position: this.props.position,
            routineId: this.props.routineId, // id rutina + ejercicio
        }
    }

    componentWillMount() {
        // console.log(this.state.position)
        this.initializeSets()
    }

    initializeSets = async () => {
        const sets = await AsyncStorage.getItem(this.state.routineId)
        const SETS = JSON.parse(sets)
        if(!SETS) {
            const newSet = [{ 'kg': '', 'reps': '' }]
            await AsyncStorage.setItem(this.state.routineId, JSON.stringify(newSet))
            this.setState({sets: newSet})
        }else {
            this.setState({sets: SETS})
        }
    }

    removeSet() {
        removeSet = true
        setTimeout(() => {
            this.initializeSets()
        })

        // this.setState({removeSet: !this.state.removeSet})
        console.log('remove set', this.state.removeSet)
    }

    addSet = async () => {
        const ROUTINE_ID = this.state.routineId
        const SETS = await AsyncStorage.getItem(ROUTINE_ID)
        const parse = JSON.parse(SETS)
        parse.push({'kg': '', 'reps': ''})
        this.setState({sets: parse})
        await AsyncStorage.setItem(ROUTINE_ID, JSON.stringify(parse))
    }

    cancelRemove() {
        removeSet = false
        setTimeout(() => {
            this.initializeSets()
        })
    }

    render() {
        const sets = this.state.sets
        return (
            <View style = {{width: '100%',  backgroundColor: '#fff',}}>
                <View style = {{width: '100%', height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',}}>
                    <View style = {{width: '15%', height: 50}}></View>
                    <View style = {{width: '35%', height: 50, justifyContent: 'center'}}><Text style = {{fontSize: 24, textAlign: 'center'}}>Kg</Text></View>
                    <View style = {{width: '35%', height: 50, justifyContent: 'center'}}><Text style = {{fontSize: 24, textAlign: 'center'}}>Rep.</Text></View>
                    {
                        removeSet ? 
                    
                            <View style = {{width: '15%', height: 50, justifyContent: 'center'}}><Image source = {require('../../../img/icons/navigation/outline_delete_black_48dp.png')} style = {{width: 30, height: 30}}/></View>

                        : 

                            <View style = {{width: '15%', height: 50, justifyContent: 'center'}}><Image source = {require('../../../img/icons/navigation/baseline_done_black_48dp.png')} style = {{width: 30, height: 30}}/></View>



                    }
                </View>
                <View style = {{width: '100%', justifyContent: 'space-between',}}>
                    <View style = {{width: '100%'}}>    
                        <RenderSets list = {sets} position = {this.state.position} routineId = {this.state.routineId} removeSet = {this.state.removeSet} />
                    </View>
                    <ExerciceButtons textLeft= 'Add set' textRight = 'Remove set'
                        imageLeft = {require('../../../img/icons/navigation/baseline_add_white_48dp.png')}
                        imageRight = {require('../../../img/icons/navigation/baseline_remove_white_48dp.png')}
                        colorLeft = {styles.green}
                        colorRight =  {styles.red}
                        actionRight = {() => this.removeSet()}
                        actionLeft = {() => this.addSet()}
                        neutralAction = {() => this.cancelRemove()} />
                </View>
            </View>
        )
    }
}

class Exercice extends Component {

    constructor(props) {
        super(props);    
        this.state = {
            activeRoutine: false,
            activeOptions: false,
            images: [],
        }
    }    

    componentWillMount() {
        let images = this.props.images
        const opened = this.props.opened
        if(opened) this.setState({activeRoutine: true})
        // console.log('opened', opened)
        this.setState({images: images})
        this.renderImages() 
    }

    renderImages() {
        const images = this.state.images
        let imagesComponent = []
        for(let i = 0; i < this.state.images.length; i++) {
            let id = images[i]
            const image = EXERCICES.image[0]
            const img = <View key = { (Math.random() * 100 ).toString()} style = {styles.image}>
                            <Image source = {image} style = {{width: 100, height: 100, borderRadius: 10,}} />
                        </View>
            imagesComponent.push(img)
        }
        return imagesComponent
    }

    toggleExercice() {
        this.setState({activeRoutine: !this.state.activeRoutine, activeOptions: false})
        setTimeout(() => {
            if(this.state.activeRoutine){
                this.modifyRecentExercices()
            }
        })
    }

    modifyRecentExercices = async () => {
        try {
            const RECENT_EXERCICES = await AsyncStorage.getItem('@Pump:RECENT_EXERCICES')
            const parse = JSON.parse(RECENT_EXERCICES)
            
            const routineId = this.props.routineId
            if(!parse) {
                const NEW_RECENT_EXERCICES = []
                NEW_RECENT_EXERCICES.push(routineId)
                await AsyncStorage.setItem('@Pump:RECENT_EXERCICES', JSON.stringify(NEW_RECENT_EXERCICES))
            }else {
                const LENGTH = parse.length
                const checkIfRepeated = this.checkRepeatedExercices(parse, routineId)
                if(!checkIfRepeated) {
                    if(LENGTH == 10){
                        parse.splice(0,1)
                        parse.push(routineId)
                    }else{
                        parse.push(routineId)
                    }
                    
                }
                await AsyncStorage.setItem('@Pump:RECENT_EXERCICES', JSON.stringify(parse))
            }

            // console.log('recent exercice', parse)
        }catch(error) {
            console.log('ERROR SETTING RECENT EXERCICES', error.message)
        }
    }


    checkRepeatedExercices(array, newItem) {
        let counter = 0
        for (let i = 0; i < array.length; i++) {
            let arrayItem = array[i]
            if(arrayItem == newItem) {
                counter++
            }
        }
        if(counter == 0) {
            return false
        }else {
            return true
        }
    }

    render() {
        return (
            <View style = {{width: '100%', alignItems: 'flex-start', justifyContent: 'flex-start',}}>
                <View style = {styles.exercice}>
                    <TouchableOpacity style = {{width: '90%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} onPress = {() => this.toggleExercice()}>
                        <View style = {styles.imageContainer}>
                            <Image source = {this.state.images[0]} style = {styles.exerciceImage}/>
                        </View>                        
                        <View style = {styles.exerciceDetails}>
                            <Text style = {{fontWeight: 'bold', fontSize: 20, width: '78%',}} numberOfLines = {3} >{this.props.exerciceName}</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {() => this.setState({activeOptions: !this.state.activeOptions, activeRoutine: false})} style = {{width: '10%', alignItems: 'center'}}>
                        <View style = {[styles.exerciceMore, {flexDirection: 'row', justifyContent: 'center',  alignItems: 'center', width: '100%', height: '100%' }]}>
                                <Image source = {require('../../../img/icons/navigation/outline_more_vert_black_48dp.png')} style = {{width: 10, height: 24}}/>
                        </View>
                    </TouchableOpacity>
                </View>
                {this.state.activeRoutine ? <ExerciceDetails position = {this.props.position} routineId = {this.props.routineId} /> : null }
                {this.state.activeOptions ? <ExerciceOptions images = {this.state.images} /> : null }
            </View>
        )
    }
}


class RenderExercices extends Component {

    constructor(props) {
        super(props);
        this.state = {
            routineId: this.props.routineId,
            exerciseId: this.props.selectedExercise
        }
    }

    _renderExercices = (item) =>  {
        const ID = item.item.id
        let opened = false
        if( ID == this.state.exerciseId) {
            opened = true
        }
        // console.log('ID is opened: ', ID + ' ' + opened + this.state.exerciseId)

        const EXERCICE = API.getExerciceWithId(ID)
        const NAME = EXERCICE.name
        const LEVEL = EXERCICE.level
        const IMAGES = EXERCICE.image
        const position = (item.index + 1).toString()
        return (
            <Exercice {...item} 
                images = {IMAGES} 
                position = {position} 
                exerciceName = {NAME} 
                id = {ID} 
                routineId = {this.state.routineId + '_' + ID}
                opened = {opened}/>
        )
    }
    
    itemSeparator = () => <View style = {{width: '100%', height: 10}}></View>

    renderEmpty = () => <View style = {{width: '100%', borderRadius: 10, alignItems: 'center',  backgroundColor: 'rgba(255,255,255,0.4)', paddingVertical: 20, paddingHorizontal: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style = {{color: '#fff', fontSize: 18, textAlign: 'center'}}>This routine has been deleted</Text>
                        </View>

    render() {
        return (
            <View style = {{width: '100%', height: '100%', paddingHorizontal: 0, }}>
                <FlatList   
                    keyExtractor = {(item, index) => item.id}
                    data = {this.props.list}
                    ListEmptyComponent = {this.renderEmpty}
                    ItemSeparatorComponent = {this.itemSeparator}
                    renderItem = {this._renderExercices}/>
            </View>
        )
    }
}


export default class ExistingRoutineScreen extends Component {

 
    state = {


    }

    constructor(props) {
    super(props);
    this.state = {
        routineId: '',
        exercices: '',
        routineDate: '',
        routineName: '',
        routineSets: '',
        exerciseId: '',
        exercisePosition: '',

    start: true,

      timerStart: false,
      stopwatchStart: false,
      totalDuration: 90000,
      timerReset: false,
      stopwatchReset: false,

    };
    this.toggleTimer = this.toggleTimer.bind(this);
    this.resetTimer = this.resetTimer.bind(this);
    this.toggleStopwatch = this.toggleStopwatch.bind(this);
    this.resetStopwatch = this.resetStopwatch.bind(this);
  }
 
  toggleTimer() {
    this.setState({timerStart: !this.state.timerStart, timerReset: false});
  }
 
  resetTimer() {
    this.setState({timerStart: false, timerReset: true});
  }
 
  toggleStopwatch() {
    this.setState({stopwatchStart: true, stopwatchReset: false, start: false});
    console.log(this.state.start)


  }
 
  resetStopwatch() {
    this.setState({stopwatchStart: false, stopwatchReset: true,  start: true});
    console.log(this.state.start)


  }
  
  getFormattedTime(time) {
      this.currentTime = time;
  }

    componentWillMount() {
        const { navigation } = this.props;

        const itemId = navigation.getParam('id');
        const exerciseId = navigation.getParam('exerciseId');


        if(itemId) {
            this.setState({routineId: itemId})
            // console.log('si routine id', itemId)
        }else {
            // console.log('no routine id')
        } 
        if(exerciseId){
            this.setState({exerciseId: exerciseId})

            // console.log('si exercise id', exerciseId)
        }else {
            // console.log('no exercise id')
        }
        
        this._retrieveData()

        
    }


    _storeData = async () => {
        try {
            await AsyncStorage.setItem('@Pump:NEW_ROUTINE', JSON.stringify(array));
        } catch (error) {
            console.log('ERROR SETTING ITEM: ', error)
        }
    }

    setScrollViewPosition(param) {

        if(param == 1) {
        // console.log(this.state.exercisePosition)

            this.setState({exercisePosition: 0})
            return
            
        }
        return 
    }

    _retrieveData = async () => {
      try {
        const MY_ROUTINES = await AsyncStorage.getItem('@Pump:MY_ROUTINES');
        const parse = JSON.parse(MY_ROUTINES)
        const ID = this.state.routineId
        let DATE = '', NAME = '', EXERCICES = [], SETS = []
        for(let i = 0; i < parse.length; i++) {
            if(parse[i].id == ID ){
                EXERCICES = parse[i].exercices
                DATE = parse[i].date
                NAME = parse[i].name
                break
            }
        }

        for(let i = 0; i < EXERCICES.length; i++) {
            // console.log('id for', EXERCICES[i].id)
            if(EXERCICES[i].id == this.state.exerciseId ){
                let position = i 
                this.setState({exercisePosition: position + 1})
                break
            }
        }

        this.setState({exercices: EXERCICES})
        this.setState({routineDate: DATE})
        this.setState({routineName: NAME})
       }catch (error) {
            // console.log('ERROR RETRIEVING ITEM: ', error.message)         
       }
    }

    deleteExercice() {
        Alert.alert(
            'Are you sure you want to delete this routine?',
            '',
            [
                {text: 'Delete', onPress: () => this.deleteRoutine(),  style: 'cancel'},
                {text: 'Cancel', }
            ],
            { cancelable: true }
        )
        
    }

    deleteRoutine = async () => {
        const MY_ROUTINES = await AsyncStorage.getItem('@Pump:MY_ROUTINES')
        const parse = JSON.parse(MY_ROUTINES)
        // console.log('my routines', parse)
        let routinePosition = 0

        for(let i = 0; i < parse.length; i++) {
            if(parse[i].id == this.state.routineId) {
                routinePosition = i
            }
        }
        parse.splice(routinePosition, 1)
        await AsyncStorage.setItem('@Pump:MY_ROUTINES', JSON.stringify(parse))
        // console.log('New pack of routines', parse)
        this.props.navigation.navigate('Home', {refreshData: true})
    }



    render() {

        return (
            <ImageBackground source = {require('../../../img/prozis.png')} style={{width: '100%', height: '100%'}} >
                
                    <View style = {styles.container}>
                        <View style = {{width: '100%', marginBottom: 0, height: 30, flexDirection: 'row',justifyContent: 'space-between', alignItems: 'flex-start'}}>
                            <View style= {{width: '25%'}}>
                                <TouchableOpacity style = {{width: '100%', height: 50, marginTop: 5}} onPress = {() => this.props.navigation.goBack()} >
                                    <View style = {{width: '100%', height: 50, flexDirection: 'row', flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start'}}>
                                        <Image style = {{height: 35, marginTop: -5,  width: 35}} source = {require('../../../img/icons/navigation/baseline_chevron_left_white_48dp.png')} />
                                        <Text style = {{color: '#fff', fontSize: 22}}>Back</Text>
                                    </View>
                                </TouchableOpacity> 
                            </View>
                            <View style = {{width: '40%'}}>
                                <TouchableOpacity activeOpacity = {0.6} >
                                     <Stopwatch laps msecs menu start={this.state.stopwatchStart}
                                                reset={this.state.stopwatchReset}
                                                options={options}
                                                getTime={this.getFormattedTime} />
                                </TouchableOpacity>
                            </View>
                            <View style = {{width: '25%', alignItems: 'flex-end'}}>
                                <TouchableOpacity onPress = {() => this.deleteExercice()} activeOpacity = {0.8} style = {{height: 50, marginRight: 10}}>
                                <View style= {{flexDirection: 'row', alignItems: 'center'}}></View>
                                    <Image style = {{width: 30, height: 30}} source = {require('../../../img/icons/navigation/outline_delete_white_48dp.png')} />
                                </TouchableOpacity>    
                            </View>                       
                        </View>
                        <View style = {{width: '100%', paddingHorizontal: 10, flexDirection: 'row', justifyContent: 'center', paddingVertical: 5}}>
                        {
                            this.state.start 
                            ?
                            <TouchableOpacity onPress = {this.toggleStopwatch} style = {{backgroundColor: 'green', width: 100, height: 30, borderRadius: 10, }}>
                                <View style = {{height: 30, width: 100, alignItems: 'center', justifyContent: 'center'}}>
                                    <Text style = {{fontSize: 16}}>Start</Text>
                                </View>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress = {this.resetStopwatch} style = {{backgroundColor: 'orange', width: 100, height: 30, borderRadius: 10, }}>
                                <View style = {{height: 30, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                    <Text style = {{fontSize: 16}}>Reset</Text>
                                </View>
                            </TouchableOpacity>   
                        }
                        </View>

                        <ScrollView style = {{height: '150%',}} >
                            <View style = {{width: '100%', marginBottom: 10, justifyContent: 'flex-start',}}>
                                <Text style = {{fontSize: 32, fontWeight: 'bold',  color: '#fff', textAlign: 'left', paddingHorizontal: 10}} numberOfLines = {1} >{this.state.routineName}</Text>
                            </View>                        
                            <RenderExercices list = {this.state.exercices} routineId = {this.state.routineId} selectedExercise = {this.state.exerciseId}/>
                            <View style = {{width: '100%', marginBottom: 10, height: keyboardSpace }}></View> 
                        </ScrollView>
                        
                    </View>                    
                
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '150%',
        paddingTop: 30,
        // marginBottom: 200,
        flex: 1,
        alignItems: 'center',
    },
    exercice: {
        width: '100%',
        height: 110,
        // padding: 10,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    /*    shadowColor: '#000000',
        shadowOpacity: 0.6,
        shadowRadius: 3,
        shadowOffset: {
            width: 2,
            height: 2
        },
        elevation: 3*/

    },
    imageContainer: {
        width: 110,
        height: 110,
        padding: 5,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    image: {
        width: 200, 
        height: 200, 
        borderRadius: 10,
        marginRight: 10,
        elevation: 3,
        shadowColor: '#000',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
            width: 1,
            height: 1
        },
    },
    exerciceImage: {
        minWidth: '20%',
        width: 100,
        height: 100,
        margin: '2.5%'
    },
    exerciceDetails: {
        height: '100%',
        // width: '100%',
        flexDirection: 'row',
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'flex-start',
        
    },
    exerciceMore: {
        minWidth: '10%',
        width: 30,        
        height: '100%',
    },
    red: {
        backgroundColor: 'rgba(255,0,0,0.65)'
    },
    gray: {
        backgroundColor: 'rgba(0,0,0,0.56)'
    },
    green: {
        backgroundColor: 'rgba(15,133,2,0.65)'
    }
})


const options = {
  container: {
    // padding: 5,
    marginLeft: -10,
    height: 40,
    borderRadius: 5,
    width: 150,
    marginTop: 0, 
  },
  text: {
    fontSize: 28,
    color: '#FFF',
    textAlign: 'center'
  }
};



// // 
//                                     contentInset = {{top: this.state.exercisePosition * -110}}
//                                     onScrollBeginDrag = {() => this.setState({exercisePosition: 0})}