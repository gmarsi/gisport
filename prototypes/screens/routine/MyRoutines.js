import React, { Component } from 'react'
import { View, Text, StyleSheet, ScrollView, Image, TouchableOpacity, FlatList, AsyncStorage, Button,ImageBackground,StatusBar, RefreshControl} from 'react-native'
import API from '../../API'

import SideMenu from 'react-native-side-menu';
import Menu from '../../components/menu/Menu';
import Routine from '../../components/routines/Routine'

import { createStackNavigator } from 'react-navigation'
import { withNavigation } from 'react-navigation';


class RenderRoutines extends Component {

     _renderRoutines = (item) =>  {
        const length = item.item.exercices.length
        const name = item.item.name
        const id = item.item.id
        let images = []
        for (let i = 0; i < length; i ++) {
            const imageId = item.item.exercices[i]
            images.push(imageId)
        }
        return (
            <Routine key = {id} {...item} name = {name} images = {images}  id = {id} length = {length}/>
        )
    }

    renderEmpty = () => <View style = {{marginTop: 5, marginHorizontal: 10, borderRadius: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(255,255,255,0.4)', paddingVertical: 40}}>
                            <Text style = {{color: '#fff', textAlign: 'center', fontSize: 18}}>You do not have routines yet. Press</Text>
                            <TouchableOpacity activeOpacity = {0.6} onPress = {this.props.onPressEmptyState} style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', marginVertical: 10}}>
                                <Text style = {{color: '#fff', textAlign: 'center', fontSize: 24}}>New routine</Text>
                                <Image source = {require('../../../img/icons/navigation/outline_add_box_white_48dp.png')} style = {{width: 25, height: 25, marginLeft: 5}}/>
                            </TouchableOpacity>
                            <Text style = {{color: '#fff', textAlign: 'center', fontSize: 18}}>to create a new one</Text>
                        </View>
    
    itemSeparator = () => <View style = {{width: '100%', height: 20}}></View>

    render() {
        return (
            <View style = {{paddingBottom: 30}}>
                <FlatList   
                    keyExtractor = {item => item.id}
                    data = {this.props.list}
                    ListEmptyComponent = {this.renderEmpty}
                    ItemSeparatorComponent = {this.itemSeparator}
                    renderItem = {item => this._renderRoutines(item)} />
                    </View>
        )
    }
}


export default class MyRoutines extends Component {

    constructor(props) {    
        super(props);
        this.state = {
            routines: [],
            isOpen: false,
            refreshing: false
        }
    }

    componentWillMount() {
        this._retrieveData()
    }   

    _retrieveData = async () => {
        try {
        const ROUTINE = await AsyncStorage.getItem('@Pump:MY_ROUTINES');
        const parse = JSON.parse(ROUTINE)
        this.setState({routines: parse})
       }
       catch (error) {
            console.log('Error al mostrar rutinas ', error) 
       }
    }

    toggle() {
        this.setState({
          isOpen: !this.state.isOpen,
        });
    } 

    updateMenuState(isOpen) {
        this.setState({ isOpen });
    }

    onMenuItemSelected(item) {
        this.setState({
          isOpen: false,
          selectedItem: item,
        })
         
        const { navigate } = this.props.navigation;
        const { push } = this.props.navigation;


        switch (item)
        {
        case "Profile": navigate('Profile')
            break;
        case 'Health': alert('Health')
            break;
        case 'Training': alert('Training')
            break;
        case 'Routines': navigate('MyRoutines')
            break;
        case 'Timer': navigate('Timer')
            break;
        case 'Settings': alert('Settings')
            break;
        case 'About': navigate('About')
            break;
        case 'addRoutine': push('NewRoutine')
            break;
        case 'Exercices': navigate('Exercices')
            break;              
        default: 
            navigate('Home')
        }

    }

    onRefresh = () => {
        this.setState({refreshing: true})
        this._retrieveData()

        setTimeout(() => {
            this.setState({refreshing: false})

        })

    }

    render() {
        const routines = this.state.routines
        const imgAddRoutine = require('../../../img/icons/navigation/outline_add_box_white_48dp.png');
        const menu = <Menu onItemSelected={(item) => this.onMenuItemSelected(item)} />;
        return (
            <SideMenu
                menu={menu}
                isOpen={this.state.isOpen}
                onChange={isOpen => this.updateMenuState(isOpen)} 
                disableGestures = {true}              >
                <ImageBackground source = {require('../../../img/prozis.png')} style={{width: '100%', height: '100%'}}>
                <StatusBar barStyle = 'light-content' />
                    <View style={[styles.bodyElement]}>                                  
                        <View style = {{width: '100%', height: '100%'}}>
                           <View style = {styles.header}>
                               <TouchableOpacity onPress = {() => this.toggle()}>
                                    <Image style = {{height: 35, width: 35}} source = {require('../../../img/icons/navigation/outline_clear_all_white_48dp.png')} />
                                </TouchableOpacity>
                                <TouchableOpacity activeOpacity = {0.8} onPress = {() => this.onMenuItemSelected('addRoutine')}>
                                    <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', marginRight: 10, paddingTop: 5, height: 40}}>
                                        <Text style= {{fontSize: 24, color: '#fff', marginRight: 5}}>New routine</Text>
                                        <Image source = {imgAddRoutine} style = {{width: 30, height: 30}} />    
                                    </View>                                
                                </TouchableOpacity>                              
                            </View>
                                <ScrollView style = {{paddingBottom: 170, height: '120%'}} refreshControl = {
                                <RefreshControl refreshing = {this.state.refreshing} onRefresh = {this.onRefresh} /> }>
                                    <View>
                                        <RenderRoutines list = {routines} onPressEmptyState = {() => this.props.navigation.navigate('NewRoutine')}/>
                                    </View>                                     
                                </ScrollView>                               
                        </View>     
                    </View>
                </ImageBackground>
            </SideMenu>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 50,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    bodyElement: {
        marginTop: 20,
        width: '100%',
        height: '100%',

    },
    bodyElementTitle: {
        paddingLeft: 10,
        marginBottom: 5,
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        shadowOffset: {height: 2},
        shadowColor: '#000',
        shadowRadius: 4,
        shadowOpacity: 0.6
    },
    
    
})
