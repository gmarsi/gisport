import React, { Component } from 'react'
import { View, Text, StyleSheet, ScrollView, Image, TouchableOpacity, FlatList, AsyncStorage, Button, ImageBackground} from 'react-native'
import API from '../../API'

import SideMenu from 'react-native-side-menu';
import Menu from '../../components/menu/Menu';

import { withNavigation } from 'react-navigation';

import LANG from '../../global'

class Routine extends Component {

    constructor(props) {
      super(props);    
      this.state = {
        id: this.props.id,
        name: this.props.name,
        images: this.props.images,
        imagesComponent: null,
        length: this.props.length,
        lang: ''
      };
    }

    componentWillMount() {
        this.setState({lang: LANG.getLang()})
        this.renderImages()
    }

    renderImages() {
        const images = this.state.images
        let imagesComponent = []
        for(let i = 0; i < this.state.length; i++) {
            let id = images[i].id
            const EXERCICES = API.getExerciceWithId(id)
            const image = EXERCICES.image[0]
            const img = <View key = { (Math.random() * 100 ).toString()} style = {styles.image}>
                            <TouchableOpacity onPress = {() => this.props.navigation.navigate('ExistingRoutine', {id: this.state.id})} activeOpacity = {1}>
                                <Image source = {image} style = {{width: 100, height: 100, borderRadius: 10,}} />
                            </TouchableOpacity>
                        </View>
            imagesComponent.push(img)
        }
        return imagesComponent
    }

    render() {
        return(
            <View style = {{width: '100%', height: 150,}}>
                <View style = {styles.routineHeader}>
                    <Text style = {{fontSize: 22, color: '#fff', width: '75%'}} numberOfLines = {1} >{this.state.name}</Text>
                    {this.state.lang['_55'] == 'ES' ?
                        <Text onPress = {() => this.props.navigation.navigate('ExistingRoutine', {id: this.state.id})} color = '#fff' style = {{fontSize: 22, fontWeight: 'bold', color: '#fff'}} >
                            Ver
                        </Text>
                        :
                        <Text onPress = {() => this.props.navigation.navigate('ExistingRoutine', {id: this.state.id})} color = '#fff' style = {{fontSize: 22, fontWeight: 'bold', color: '#fff'}} >
                            View
                        </Text>
                }

                </View>
                <View style = {{width: '100%', overflow: 'hidden',  backgroundColor: 'rgba(255,255,255,0.3)', paddingHorizontal: 5}}>
                    <ScrollView horizontal = {true}>
                        <View style = {styles.routineImages}>
                                {this.renderImages()}
                        </View>                    
                    </ScrollView>
                </View>
            </View>
        )
    }
}

export default withNavigation(Routine)

const styles = StyleSheet.create({
    image: {
        width: 100, 
        height: 100, 
        borderRadius: 10,
        marginRight: 10,
        elevation: 3,
        shadowColor: '#000',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
            width: 1,
            height: 1
        },
    },
    routineHeader: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
        paddingHorizontal: 10,
        overflow: 'hidden'
    },
    routineImages: {
        width: '100%',
        height: 120 ,
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    
})