import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Image, AsyncStorage} from 'react-native'
import LANG from '../../global'


export default class Header extends Component {
    constructor(props) {
      super(props);    
      this.state = {
        lang: ''
      };
    }
    componentWillMount() {
        this.setState({lang: LANG.getLang()})
    }
    render() {
        const ImgMenu = require('../../../img/icons/navigation/outline_clear_all_white_48dp.png');
        const imgAddRoutine = require('../../../img/icons/navigation/outline_add_box_white_48dp.png');
        return(
            <View style = {styles.header}>
                <View style={[styles.headerRow, styles.headerOptionsContainer]}>
                    <View>
                        <TouchableOpacity onPress = {this.props.toggleMenu} activeOpacity = {0.8}>
                            <Image style = {{width: 35, height: 35}} source = {ImgMenu} />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity activeOpacity = {0.8} onPress = {this.props.addRoutine} style = {{height: 40}}>
                        <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', marginRight: 10, paddingTop: 5, height: 40,}}>
                            {this.state.lang['_55'] == 'ES' ?
                                <Text style= {{fontSize: 24, color: '#fff', marginRight: 5}}>Nueva Rutina</Text>
                                :
                                <Text style= {{fontSize: 24, color: '#fff', marginRight: 5}}>New routine</Text>
                            }
                            
                            <Image source = {imgAddRoutine} style = {{width: 30, height: 30}} />    
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        paddingTop: 20,
        height: 50,
        marginBottom: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerOptionsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: '15%',
        position: 'absolute',
        top: 30,
        left: 5,
        marginBottom: 15
    },
    headerRow: {
        width: '100%',
    },
})