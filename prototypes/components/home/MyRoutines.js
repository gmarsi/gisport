import React, { Component } from 'react'
import { View, Text, StyleSheet, ScrollView, Image, TouchableOpacity, FlatList, AsyncStorage, Button} from 'react-native'
import API from '../../API'
import Routine from '../routines/Routine'
import LANG from '../../global'

class RenderRoutines extends Component {

    constructor(props) {
      super(props);    
      this.state = {
        lang: this.props.lang
      };
    }

    _renderRoutines = (item) =>  {        
        const length = item.item.exercices.length
        const name = item.item.name
        const id = item.item.id
        let images = []
        for (let i = 0; i < length; i ++) {
            const imageId = item.item.exercices[i]
            images.push(imageId)
        }
        return (
            <Routine {...item} name = {name} images = {images}  id = {id} length = {length}/>
        )
    }

    renderEmpty = () => <View style = {{marginTop: 5, marginHorizontal: 10, borderRadius: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(255,255,255,0.4)', paddingVertical: 40}}>
                            {
                                this.state.lang['_55'] = 'ES' ?
                                    <Text style = {{color: '#fff', textAlign: 'center', fontSize: 18}}>Aún no tienes rutinas. Pulsa</Text>
                                    :
                                    <Text style = {{color: '#fff', textAlign: 'center', fontSize: 18}}>You do not have routines yet. Press</Text>
                            }

                            
                            <TouchableOpacity activeOpacity = {0.6} onPress = {this.props.onPressEmptyState} style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', marginVertical: 10}}>
                            {
                                this.state.lang['_55'] = 'ES' ?
                                    <Text style = {{color: '#fff', textAlign: 'center', fontSize: 24}}>Nueva rutina</Text>
                                    :
                                    <Text style = {{color: '#fff', textAlign: 'center', fontSize: 24}}>New routine</Text>
                            }

                            <Image source = {require('../../../img/icons/navigation/outline_add_box_white_48dp.png')} style = {{width: 25, height: 25, marginLeft: 5}}/>
                            </TouchableOpacity>
                            {
                                this.state.lang['_55'] = 'ES' ?
                                    <Text style = {{color: '#fff', textAlign: 'center', fontSize: 18}}>para crear una nueva</Text>
                                    :
                                    <Text style = {{color: '#fff', textAlign: 'center', fontSize: 18}}>to create a new one</Text>
                            }
                            
                        </View>
    
    itemSeparator = () => <View style = {{width: '100%', height: 20}}></View>

    render() {
        return (
            <View style = {{width: '100%', height: '100%', paddingHorizontal: 0, }}>
                <FlatList   
                    keyExtractor = {item => item.id}
                    data = {this.props.list}
                    ListEmptyComponent = {this.renderEmpty}
                    ItemSeparatorComponent = {this.itemSeparator}
                    renderItem = {this._renderRoutines}/>
            </View>
        )
    }
}


export default class MyRoutines extends Component {

    constructor(props) {    
        super(props);
        this.state = {
            routines: [],
            lang: ''
        }
    }

    componentWillMount() {
        this.setState({lang: LANG.getLang()})
        this._retrieveData()
    }

    componentWillReceiveProps() {
        this._retrieveData()
    }

    _retrieveData = async () => {
        try {
            const ROUTINE = await AsyncStorage.getItem('@Pump:MY_ROUTINES');
            const parse = JSON.parse(ROUTINE)
            this.setState({routines: parse})
        }
        catch (error) {
            console.log('Error al mostrar rutinas ', error) 
        }
    }

    render() {
        const routines = this.state.routines
        return (
            <View style={[styles.bodyElement, {justifyContent: 'flex-start'}]}>                
                <View style = {{width: '100%', flex: 1, height: '100%',}}>
                        <RenderRoutines list = {routines} onPressEmptyState = {this.props.onPressEmptyState} lang = {this.state.lang}/>
                </View>                
            </View>
        )
    }
}


const styles = StyleSheet.create({
    bodyElement: {
        marginTop: 20,
        width: '100%',
        height: '100%',
    },
    bodyElementTitle: {
        paddingLeft: 10,
        marginBottom: 5,
        width: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        shadowOffset: {height: 2},
        shadowColor: '#000',
        shadowRadius: 4,
        shadowOpacity: 0.6
    },
    routineHeader: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 30,
        paddingHorizontal: 10
    },
    routineImages: {
        width: '100%',
        height: 120 ,
        paddingVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: 100, 
        height: 100, 
        borderRadius: 10,
        marginRight: 10,
        elevation: 3,
        shadowColor: '#000',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: {
            width: 1,
            height: 1
        },
    }
})
