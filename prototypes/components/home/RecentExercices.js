import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, StyleSheet, FlatList, AsyncStorage, Dimensions } from 'react-native'
import { withNavigation } from 'react-navigation';
import API from '../../API';
import RecentElement from './RecentElement'
import LANG from '../../global'


class RenderRecentExercises extends Component {

    state = {
        lang: this.props.lang
    }

    _renderRecentExercises = (item) =>  {
        const newItem = item.item
        const stringId = newItem.split('_')
        const imageId = stringId[2]
        const exercice = API.getExerciceWithId(imageId)
        const image = exercice.image[0]        
        return (
            <RecentElement {...item} image = {image} id = {newItem}/>
        )
    }

    windowWidth() {
        const window = Dimensions.get('window')
        const width = window.width - 20
        return { width: width }
    }
    
    renderEmpty = () => <View style = {[this.windowWidth(), {marginTop: 5, borderRadius: 10, alignItems: 'center',  backgroundColor: 'rgba(255,255,255,0.4)', paddingVertical: 20, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}]}>
                            {
                                this.state.lang['_55'] == 'ES' ?
                                <Text style = {{color: '#fff', fontSize: 18}}>No hay ejercicios recientes</Text>
                                :
                                <Text style = {{color: '#fff', fontSize: 18}}>There is no recent exercice</Text>

                            }
                        </View>

    itemSeparator = () => <View style = {{width: 10, height: 200}}></View>

    render() {
        return (
            <View style= {{width: '100%',}}>
                <FlatList 
                    keyExtractor = {item => item}
                    data = {this.props.list}
                    ItemSeparatorComponent = {this.itemSeparator}
                    ListEmptyComponent = {this.renderEmpty}
                    renderItem = {this._renderRecentExercises} 
                    horizontal = {true}/>
            </View>
        )
    }
}


class RecentExercises extends Component {

    constructor(props) {
        super(props);
        this.state = {
            RECENT_EXERCISES: [],
            lang: ''
        }
    }

    componentWillMount() {
        this.setState({lang: LANG.getLang()})
        this._retrieveData()
    }

    componentWillReceiveProps() {
        this._retrieveData()
    }

    _retrieveData = async () => {
        try {
            const RECENT_EXERCISES = await AsyncStorage.getItem('@Pump:RECENT_EXERCICES')

            if(RECENT_EXERCISES) {
                const parse = JSON.parse(RECENT_EXERCISES)
                parse.reverse()
                this.setState({RECENT_EXERCISES: parse})
            }else {
                this.setState({RECENT_EXERCISES: []})
            }

        }catch(error) {
            console.log('ERROR RETRIEVING DATA FROM RECENT EXERCISES')
        }
        
    }

    render() {
        let RECENT_EXERCISES = this.state.RECENT_EXERCISES
        return(
            <View style={styles.recent}>
            {
                        this.state.lang['_55'] == 'ES' ?
                        
                        <View style={{height: '30%', paddingLeft: 5}}><Text style = {{fontSize: 24, color: '#fff', textShadowColor: 'transparent'}}>Ejercicios recientes</Text></View>
                        :
                        <View style={{height: '30%', paddingLeft: 5}}><Text style = {{fontSize: 24, color: '#fff', textShadowColor: 'transparent'}}>Recent exercises</Text></View>
            }    
                <View style={styles.recentExercise}>
                        <RenderRecentExercises list = {RECENT_EXERCISES} lang = {this.state.lang}/>           
                </View>
            </View>

        )
    }
}

export default withNavigation(RecentExercises)

const styles = StyleSheet.create({
    recent: {
        flex: 1,
        height: 160,
        marginTop: 15,
        width: '100%',

    },
    recentExercise: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: 160,
        width: '100%',
        paddingHorizontal: 10,
    },
    recentElementDetails: {
        width: '100%',
        height: 50,
        borderRadius: 10,
        backgroundColor: '#fff',
        flex: 1, 
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    textDetails: {
        fontSize: 14,
        fontWeight: 'bold' 
    }
})