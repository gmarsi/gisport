import React, { Component } from 'react'
import { View, Image, TouchableOpacity, StyleSheet, } from 'react-native'
import { withNavigation } from 'react-navigation';
import API from '../../API'

class RecentElement extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.id
        }
    }

    navigateToExercise() {

        const id = this.state.id
        console.log('id', id)
        const splice = id.split('_')
        const routineId = splice[0] + '_' + splice[1]
        const exerciseId = splice[2]

        const { navigate } = this.props.navigation
        navigate('ExistingRoutine', {exerciseId: exerciseId, id: routineId})
    }

    render() {
        return (
            <TouchableOpacity onPress = {() => this.navigateToExercise()} activeOpacity = {0.9}>
                <View style={styles.recentElement}>
                    <Image source = {this.props.image} style = {{ width: 150, height: 150, borderRadius: 10}}/>
                </View>
            </TouchableOpacity>

        )
    }
}

export default withNavigation(RecentElement)

const styles = StyleSheet.create({
    recentElement: {
        height: 160,
        width: 160,
        backgroundColor: '#fff',
        borderRadius: 10,
        padding: 5,
    },
})