import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, AsyncStorage} from 'react-native'
import { withNavigation } from 'react-navigation';
import MyRoutines from './MyRoutines'

import LANG from '../../global'

// var LANG = ''

class Body extends Component {

    constructor(props) {
      super(props);    
      this.state = {
        lang: ''
      };
    }

    static navigationOptions = {
        header: null
    }

    componentWillMount() {
        this.setState({lang: LANG.getLang()})
        // setTimeout(() => {
        //     console.log('body', this.state.lang)
        // }, 1000)
    }

    onRoutineSelected(item) {
        switch (item) {
            case "ViewRoutine":
                this.props.navigation.navigate('ViewRoutine')
                break;
        }
    }

    render() {
        return(
            <View style={styles.body}>
                    <View style = {{width: '100%', height: 40, marginBottom: 20}}>
                    {
                        this.state.lang['_55'] == 'ES' ?
                        <Text style = {{fontSize: 24, color: '#fff', textShadowColor: 'transparent', paddingLeft: 10}}>Rutinas</Text>
                        :
                        <Text style = {{fontSize: 24, color: '#fff', textShadowColor: 'transparent', paddingLeft: 10}}>Routines</Text>

                    }
                        
                    }
                    </View>
                     <View style = {{width: '100%', height: '100%', justifyContent: 'space-around'}}>
                        <MyRoutines onPressEmptyState = {() => this.props.navigation.navigate('NewRoutine')} />
                    </View>
            </View>
        )   
    }
}

export default withNavigation(Body)

const styles = StyleSheet.create({
    body: {
        flexDirection: 'column',
        width: '100%',
        height: '100%',
        marginTop: 50,
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'space-around',
        paddingBottom: 10,
    },
})