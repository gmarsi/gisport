import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Dimensions,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  TouchableOpacity
} from 'react-native';

import LANG from '../../global'

const window = Dimensions.get('window');

class MenuElement extends Component {
  render() {
    return( 
      <TouchableOpacity activeOpacity = {0.6} onPress = { this.props.onPress} style = {[styles.menuElement, this.props.style]}>
          <Image style = {styles.menuElementImage} source = {this.props.icon}  />
          <Text style= {styles.menuElementText}>{this.props.text}</Text>
        </TouchableOpacity>
    )
  }
}

export default function Menu({ onItemSelected }) {
  state = {
    lang: LANG.getLang()
  }

      // this.setState({lang: LANG.getLang()})
      setTimeout(()=> {
        console.log('menu', this.state.lang)
      })




  return (
    <ScrollView scrollsToTop = { false } style = { styles.menu }>
      <View style = {styles.menuContent }>
          {/*<View style = { styles.avatarContainer }>
            <Image style = { styles.avatar } source = { require('../../../img/bkgr.png') } />
            <Text style = { styles.name }>Marcos gimenez</Text>
          </View>*/}
          {this.state.lang['_55'] = 'ES' ?
            <View style = {{paddingTop: 50, justifyContent: 'space-between', height: '100%'}}>
              <View>
                <MenuElement text = 'Inicio' icon = {require('../../../img/icons/navigation/outline_home_white_48dp.png')} onPress = {() => onItemSelected('Home')}/>
                <MenuElement text = 'Perfil' icon = {require('../../../img/icons/navigation/outline_account_circle_white_48dp.png')} onPress = {() => onItemSelected('Profile')}/>
                {/*<MenuElement text = 'Health' icon = {require('../../../img/icons/navigation/outline_favorite_border_white_48dp.png')} onPress = {() => onItemSelected('Health')}/>*/}
                <MenuElement text = 'Rutinas' icon = {require('../../../img/icons/navigation/baseline_date_range_white_48dp.png')} onPress = {() => onItemSelected('Routines')}/>
                <MenuElement text = 'Cronómetro' icon = {require('../../../img/icons/navigation/outline_timer_white_48dp.png')} onPress = {() => onItemSelected('Timer')}/>
                <MenuElement text = 'Ejercicios' icon = {require('../../../img/icons/navigation/outline_fitness_center_white_48dp.png')} onPress = {() => onItemSelected('Exercices')}/>
                <MenuElement text = 'Clear' icon = {require('../../../img/icons/navigation/outline_delete_white_48dp.png')} onPress = {() => onItemSelected('Clear')}/>
               {/* <MenuElement text = 'Settings' icon = {require('../../../img/icons/navigation/outline_settings_white_48dp.png')} onPress = {() => onItemSelected('Settings')}/>*/}
              </View>
              <View>
                <MenuElement style = {{marginBottom: 20}} text = 'Acerca de' icon = {require('../../../img/icons/navigation/outline_info_white_48dp.png')} onPress = {() => onItemSelected('About')}/>
                <View style = {{width: '60%', height: 60, paddingRight: 10, paddingBottom: 30}}>
                  <Image source = {require('../../../img/logos/pump.png')} style = {{flex: 1, width: null, height: null, resizeMode: 'contain'}}/>
                </View>
              </View>
            </View>

            :
            <View style = {{paddingTop: 50, justifyContent: 'space-between', height: '100%'}}>
              <View>
                <MenuElement text = 'Home' icon = {require('../../../img/icons/navigation/outline_home_white_48dp.png')} onPress = {() => onItemSelected('Home')}/>
                <MenuElement text = 'Profile' icon = {require('../../../img/icons/navigation/outline_account_circle_white_48dp.png')} onPress = {() => onItemSelected('Profile')}/>
                {/*<MenuElement text = 'Health' icon = {require('../../../img/icons/navigation/outline_favorite_border_white_48dp.png')} onPress = {() => onItemSelected('Health')}/>*/}
                <MenuElement text = 'Routines' icon = {require('../../../img/icons/navigation/baseline_date_range_white_48dp.png')} onPress = {() => onItemSelected('Routines')}/>
                <MenuElement text = 'Timer' icon = {require('../../../img/icons/navigation/outline_timer_white_48dp.png')} onPress = {() => onItemSelected('Timer')}/>
                <MenuElement text = 'Exercices' icon = {require('../../../img/icons/navigation/outline_fitness_center_white_48dp.png')} onPress = {() => onItemSelected('Exercices')}/>
                {/*<MenuElement text = 'Clear' icon = {require('../../../img/icons/navigation/outline_delete_white_48dp.png')} onPress = {() => onItemSelected('Clear')}/>
                <MenuElement text = 'Settings' icon = {require('../../../img/icons/navigation/outline_settings_white_48dp.png')} onPress = {() => onItemSelected('Settings')}/>*/}
              </View>
              <View>
                <MenuElement style = {{marginBottom: 20}} text = 'About' icon = {require('../../../img/icons/navigation/outline_info_white_48dp.png')} onPress = {() => onItemSelected('About')}/>
                <View style = {{width: '60%', height: 60, paddingRight: 10, paddingBottom: 30}}>
                  <Image source = {require('../../../img/logos/pump.png')} style = {{flex: 1, width: null, height: null, resizeMode: 'contain'}}/>
                </View>
              </View>
            </View>
        }


        
      </View>
    </ScrollView>

  );
}


const styles = StyleSheet.create({
  menu: {
    flex: 1,
    width: 320,
    height: window.height,
    backgroundColor: 'rgb(40,40,40)',
    paddingLeft: 20,
    height: '100%'
  },
  avatarContainer: {
    marginBottom: 20,
    marginTop: 20,
    backgroundColor: 'red'
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    flex: 1, 
    backgroundColor: 'orange'                                                                                                      
  },
  name: {
    marginTop: 10,
    color: '#fff',
    fontSize: 20,
    width: '100%'
  },
  menuContent: {
    justifyContent: 'space-between',
    height: window.height
  },
  item: {
    fontSize: 14,
    fontWeight: '300',
    paddingTop: 5,
  },
  menuElement: {
    marginBottom: 30,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  menuElementImage: {
    width: 30,
    height: 30,
    marginRight: 10
  },
  menuElementText: {
    fontSize: 24,
    color: '#fff'
   }
});

Menu.propTypes = {
  onItemSelected: PropTypes.func.isRequired,
};