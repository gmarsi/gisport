import { AsyncStorage } from 'react-native'

class LANG{
	getLang = async () =>  {
		try{
			const LANG = await AsyncStorage.getItem('@Pump:LANGUAGE')
			return LANG
		}catch (error){
			console.log('Error al obtener el lenguaje Global.js', error.message)
		}

	}
}

export default new LANG()