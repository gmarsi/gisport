import React, { Component } from 'react'
import { View, FlatList, Text, TouchableOpacity, ScrollView, TextInput, StyleSheet, Image, ImageBackground, AsyncStorage} from 'react-native'

import API_EXTRACTOR from './API'



function _renderImages(props) {
   const array = props
    let imageComponent = array.map((index) => {
        return (<Image source = {index} style = {{width: 50, height: 50}}/>)
    })
    return imageComponent
}

// ITEMS
function API_EXERCICES (props) {

  return(

      <View style = {styles.container}>
          <Text>{props.name}</Text>
          <Text>{props.level}</Text>
          <Text>{props.group}</Text>
          <Text>{props.minRep}</Text>
          <Text>{props.maxRep}</Text>
          { _renderImages(props.image) }

      </View>

  )


}

//FLAT LIST
class FLAT_LIST extends Component {

    keyExtractor = item => item.id.toString()

    renderEmpty = () => <View style = {{marginTop: 50, alignItems: 'center', justifyContent: 'center'}}><Text>No hay ejercicios</Text></View>

    itemSeparator = () => <View style = {{backgroundColor: 'red', width: '100%', height: 50}}></View>

    renderItem = ({item}) => {
        return(
            <API_EXERCICES {...item} />
        )
    };


    render() {
        return(
            <View>
              <FlatList keyExtractor = {this.keyExtractor}
                        data={this.props.list}
                        ListEmptyComponent={this.renderEmpty}
                        ItemSeparatorComponent={this.itemSeparator}
                        renderItem={this.renderItem} />
            </View>
        )
    }
}





// APP
class API_TEST extends Component {

    state = {
      exercices: []
    }

    componentDidMount() {
      const EXERCICES_FROM_API = API_EXTRACTOR.getExercices()

      this.setState({exercices: EXERCICES_FROM_API})

    }
    render() {
      return(
        <FLAT_LIST list = {this.state.exercices}/>
      )
    }



    
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default API_TEST
