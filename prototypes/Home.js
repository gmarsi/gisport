import React, { Component } from 'react'
import { AsyncStorage, View, Text, TouchableOpacity, ScrollView, FlatList, StyleSheet, Image, ImageBackground, StatusBar, RefreshControl} from 'react-native'
import { LinearGradient } from 'expo'
import SideMenu from 'react-native-side-menu'
import Menu from './components/menu/Menu'
import Header from './components/home/Header'
import RecentExercices from './components/home/RecentExercices'
import Body from './components/home/Body.js'
import Routine from './components/routines/Routine'
import ProfileScreen from './screens/profile/Profile'
import LangView from './screens/language/Lang'

import { createStackNavigator } from 'react-navigation'

import CreateRoutineScreen from './screens/routine/CreateRoutine'
import ExistingRoutineScreen from './screens/routine/ExistingRoutine'
import MyRoutinesScreen from './screens/routine/MyRoutines'
import TimerScreen from './screens/timer/Timer'

import ExercicesScreen from './screens/exercices/Exercices'
import AboutScreen from './screens/about/AboutScreen'

import { NavigationEvents } from 'react-navigation'



class HomeScreen extends Component {



    static navigationOptions = {
        header: null
    }
    constructor(props) {
      super(props);
      this.toggle = this.toggle.bind(this);
      this.state = {
          isOpen: false,
          refreshing: false,
          isFocused: this.props.navigation.isFocused()
      };
    }
        

    // componentWillMount() {
    //     this.clearAll()
    // }
    clearAll = async () => {
        await AsyncStorage.clear()
    }


    onRefresh = () => {
        this.setState({refreshing: true})
        setTimeout(() => {
            this.setState({refreshing: false})
        })
    }

    toggle() {
        this.setState({
          isOpen: !this.state.isOpen,
        });
    }

    updateMenuState(isOpen) {
        this.setState({ isOpen });
    }

    onMenuItemSelected(item) {
        this.setState({
          isOpen: false,
        })
         
        const { navigate } = this.props.navigation;
        const { push } = this.props.navigation;


        switch (item)
        {
        case "Profile":
            navigate('Profile')
            break;
        case 'Health':
            alert('Health')
            break;
        case 'Training':
            alert('Training')
            break;
        case 'Routines':
            navigate('MyRoutines')
            break;
        case 'Timer':
            navigate('Timer')
            break;
        case 'Settings':
            alert('Settings')
            break;
        case 'About':
            navigate('About')
            break;
        case 'addRoutine':
            navigate('NewRoutine')
            break;
        case 'Exercices':
            navigate('Exercices')
            break;
        case 'Clear':
            this.clearAll()
            break;
        default:
            navigate('Home')
        }

    }

  render() {
    const menu = <Menu onItemSelected={(item) => this.onMenuItemSelected(item)}  />;
    return (
      <SideMenu
        menu={menu}
        isOpen={this.state.isOpen}
        onChange={isOpen => this.updateMenuState(isOpen)} 
        disableGestures = {true}
      >
        <View style={styles.containerMenu}>
        <NavigationEvents
          onDidFocus={this.onRefresh}
        />
            <StatusBar barStyle = 'light-content' />
            <ImageBackground source = {require('../img/prozis.png')} style={{width: '100%', height: '100%'}}>
                <View style = {styles.container}>
                    <Header toggleMenu = {() => this.toggle()} addRoutine = {() => this.onMenuItemSelected('addRoutine')} />
                    <ScrollView style = {[{width: '100%', height: '100%'}]} >
                        <View style={{width: '100%', justifyContent: 'center', marginBottom: 75}}>
                            <RecentExercices/>                   
                        </View>
                        <Body />
                    </ScrollView>               
                </View>
            </ImageBackground>
        </View>        
      </SideMenu>
    );
  }
}
// refreshControl = { <RefreshControl refreshing = {this.state.refreshing} onRefresh = {this.onRefresh} /> }

export default class App extends Component {    
    render() {
        return (
            <RootStack/>
        )
    }
}

const RootStack = createStackNavigator(
  {
    Home: {screen: HomeScreen},
    Profile: {screen: ProfileScreen},
    MyRoutines: {screen: MyRoutinesScreen},
    NewRoutine: {screen: CreateRoutineScreen},
    ExistingRoutine: {screen: ExistingRoutineScreen},
    Timer: {screen: TimerScreen },
    Exercices: {screen: ExercicesScreen},
    About: {screen: AboutScreen},
    Language: {screen: LangView}
    

  },
  {
    initialRouteName: 'Language',
    navigationOptions: {
      headerStyle: {
        backgroundColor: 'rgb(167,167,167)',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      header: null
    },
  }
);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    
    
    button: {
        position: 'absolute',
        top: 20,
        padding: 10,
    },
    caption: {
        fontSize: 20,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    containerMenu: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
})